package com.sample.soa.hystrix;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.netflix.hystrix.strategy.HystrixPlugins;
import com.yammer.tenacity.core.bundle.TenacityBundleBuilder;
import com.yammer.tenacity.core.bundle.TenacityBundleConfigurationFactory;
import com.yammer.tenacity.core.config.BreakerboxConfiguration;
import com.yammer.tenacity.core.config.TenacityConfiguration;
import com.yammer.tenacity.core.properties.StringTenacityPropertyKeyFactory;
import com.yammer.tenacity.core.properties.TenacityPropertyKey;
import com.yammer.tenacity.core.properties.TenacityPropertyKeyFactory;

import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 */
public final class SOAHystrixInitializer {

  /**
   * private Constructor.
   */
  private SOAHystrixInitializer() {

  }

  /**
   *
   * @param bootstrap
   *          bootstrap
   * @param tenacityBundleConfigurationFactory
   *          tenacityBundleConfigurationFactory
   */
  public static void initialize(final Bootstrap<?> bootstrap,
      final TenacityBundleConfigurationFactory<Configuration> tenacityBundleConfigurationFactory) {

    if (tenacityBundleConfigurationFactory == null) {
      bootstrap.addBundle(TenacityBundleBuilder.newBuilder()
          .configurationFactory(new TenacityBundleConfigurationFactory<Configuration>() {

            @Override()
            public Map<TenacityPropertyKey, TenacityConfiguration> getTenacityConfigurations(
                final Configuration configuration) {
              final ImmutableMap.Builder<TenacityPropertyKey,
                  TenacityConfiguration> builder = ImmutableMap
                  .builder();
              return builder.build();
            }

            @Override()
            public TenacityPropertyKeyFactory getTenacityPropertyKeyFactory(
                final Configuration configuration) {
              return new StringTenacityPropertyKeyFactory();
            }

            @Override()
            public BreakerboxConfiguration getBreakerboxConfiguration(
                final Configuration configuration) {
              return new BreakerboxConfiguration();
            }
          }).usingAdminPort().build());
    } else {
      bootstrap.addBundle(TenacityBundleBuilder.newBuilder()
          .configurationFactory(tenacityBundleConfigurationFactory).usingAdminPort().build());
      HystrixPlugins.getInstance().registerConcurrencyStrategy(new SOAHystrixConcurrencyStrategy());
    }
  }
}
