package com.sample.soa.hystrix;

import javax.ws.rs.core.Configuration;

import org.glassfish.jersey.server.model.ModelProcessor;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.model.ResourceModel;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandMetrics;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixCommandProperties.Setter;
import com.netflix.hystrix.strategy.properties.HystrixPropertiesFactory;

/**
 * Created by seedara on 4/20/16.
 *
 * @author seedara
 * @version 1.0
 *
 */
public class SOAHystrixResourceMethodInstrumenter
    implements ApplicationEventListener, ModelProcessor {

  /**
   *
   * @param event ApplicationEvent
   */
  @Override()
  public void onEvent(final ApplicationEvent event) {
    if (event.getType() == ApplicationEvent.Type.INITIALIZATION_APP_FINISHED) {
      registerMetricsForModel(event.getResourceModel());
    }
  }

  /**
   *
   * @param event RequestEvent
   * @return RequestEventListener
   */
  @Override()
  public RequestEventListener onRequest(final RequestEvent event) {
    final ResourceMethod method = event.getUriInfo().getMatchedResourceMethod();
    final RequestEventListener listener = new SOAHystrixTimerRequestEventListener();
    return listener;
  }

  /**
   *
   * @param resourceModel ResourceModel
   * @return ResourceModel
   */
  @Override()
  public ResourceModel processResourceModel(final ResourceModel resourceModel,
      final Configuration configuration) {
    return resourceModel;
  }

  /**
   *
   * @param subResourceModel ResourceModel
   * @param configuration Configuration
   * @return ResourceModel
   */
  @Override()
  public ResourceModel processSubResource(final ResourceModel subResourceModel,
      final Configuration configuration) {
    registerMetricsForModel(subResourceModel);
    return subResourceModel;
  }

  /**
   *
   * @param resourceModel ResourceMode
   */
  private void registerMetricsForModel(final ResourceModel resourceModel) {
    for (final Resource resource : resourceModel.getResources()) {
      register(resource);
    }
  }

  /**
   *
   * @param resource Resource
   */
  private void register(final Resource resource) {

    for (final ResourceMethod method : resource.getAllMethods()) {
      registerTimedAnnotations(method);
    }

    for (final Resource childResource : resource.getChildResources()) {
      for (final ResourceMethod method : childResource.getAllMethods()) {
        registerTimedAnnotations(method);
      }
    }
  }

  /**
   *
   * @param method ResourceMethod
   */
  private void registerTimedAnnotations(final ResourceMethod method) {

    final int tenSecondsInMillis = 10000;
    if (SOAHystrixTimerRequestEventListener.getCommandName(method) != null) {
      final HystrixCommandKey commandKey = HystrixCommandKey.Factory
          .asKey(SOAHystrixTimerRequestEventListener.getCommandName(method));
      final HystrixCommandGroupKey groupKey = HystrixCommandGroupKey.Factory
          .asKey(SOAHystrixTimerRequestEventListener.getCommandGroupName(method));
      final Setter hystrixSetter = HystrixCommandProperties.Setter()
          .withMetricsRollingStatisticalWindowInMilliseconds(tenSecondsInMillis)
          .withCircuitBreakerEnabled(false);
      final HystrixCommandProperties commandProperties = HystrixPropertiesFactory
          .getCommandProperties(commandKey, hystrixSetter);
      // This will create a instance if none exist.
      HystrixCommandMetrics.getInstance(commandKey, groupKey, commandProperties);
    }
  }
}
