package com.sample.soa.hystrix;

import java.util.concurrent.Callable;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 *
 */
public class SOAHystrixConcurrencyStrategy extends
             com.netflix.hystrix.strategy.concurrency.HystrixConcurrencyStrategy {

  /**
   *
   * @param callable Callable
   *
   */
  @Override()
  public Callable wrapCallable(final Callable callable) {
    return new SOAHystrixContextCallable(callable);
  }
}
