package com.sample.soa.hystrix;

import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.MDC;

/**
 *
 * @author Srinivas Posinasetty
 *
 * @version 1.0
 * @param <K> Type
 */
public class SOAHystrixContextCallable<K> implements Callable<K> {

  private final Callable<K> actual;
  private final Map<String, String> parentMdc;

  /**
   *
   * @param actual actual
   */
  public SOAHystrixContextCallable(final Callable<K> actual) {
    this.actual = actual;
    this.parentMdc = MDC.getCopyOfContextMap();
  }

  @Override()
  public K call() throws Exception {
    
    final Map<String, String> childMdc = MDC.getCopyOfContextMap();
    try {
      if (this.parentMdc != null) {
        MDC.setContextMap(parentMdc);
      }
      return actual.call();
    } finally {
      if (childMdc != null) {
        MDC.setContextMap(childMdc);
      }
    }
  }
}
