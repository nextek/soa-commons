package com.sample.soa.hystrix;

import java.lang.reflect.Method;

import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;

import com.codahale.metrics.annotation.Timed;
import com.netflix.hystrix.ExecutionResult;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixEventType;
import com.netflix.hystrix.HystrixThreadPoolKey;
import com.netflix.hystrix.metric.HystrixThreadEventStream;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 */
public class SOAHystrixTimerRequestEventListener implements RequestEventListener {

  private ExecutionResult executionResult = null;
  private HystrixCommandKey commandKey;
  private HystrixCommandGroupKey commandGroupKey;
  private HystrixThreadPoolKey threadPoolKey;

  /**
   * @param event RequestEvent
   */
  @Override()
  public void onEvent(final RequestEvent event) {

    if (event.getType() == RequestEvent.Type.RESOURCE_METHOD_START) {

      final ResourceMethod method = event.getUriInfo().getMatchedResourceMethod();
      if (getCommandName(method) != null) {

        commandKey = HystrixCommandKey.Factory.asKey(getCommandName(method));
        commandGroupKey = HystrixCommandGroupKey.Factory.asKey(getCommandGroupName(method));
        threadPoolKey = HystrixThreadPoolKey.Factory.asKey(commandGroupKey.name());

        executionResult = ExecutionResult.from();
        executionResult = executionResult.setInvocationStartTime(System.currentTimeMillis());
        HystrixThreadEventStream.getInstance().commandExecutionStarted(commandKey, threadPoolKey,
            HystrixCommandProperties.ExecutionIsolationStrategy.THREAD, 0);
      }
    } else if (event.getType() == RequestEvent.Type.FINISHED) {
      if (executionResult != null) {
        processFinishedEvent(event);
        HystrixThreadEventStream.getInstance().executionDone(executionResult, commandKey,
            threadPoolKey);
      }
    }
  }

  private void processFinishedEvent(final RequestEvent event) {

    final int userThreadLatency = (int) (System.currentTimeMillis()
        - this.executionResult.getStartTimestamp());

    if (event.getContainerResponse().getStatusInfo()
        .getFamily() == Response.Status.Family.SUCCESSFUL) {
      this.executionResult = executionResult.addEvent(userThreadLatency,
          HystrixEventType.SUCCESS);
    } else if (event.getContainerResponse().getStatusInfo()
        .getFamily() == Response.Status.Family.CLIENT_ERROR) {
      this.executionResult = executionResult.addEvent(userThreadLatency,
          HystrixEventType.BAD_REQUEST);
    } else {
      this.executionResult = executionResult.addEvent(userThreadLatency,
          HystrixEventType.FAILURE);
    }
    this.executionResult = executionResult.markUserThreadCompletion(userThreadLatency);
  }

  /**
   *
   * @param method
   *          method
   * @return String
   */
  public static String getCommandName(final ResourceMethod method) {

    final String nameToReturn;
    if (method == null) {
      return null;
    }

    final Method definitionMethod = method.getInvocable().getDefinitionMethod();
    final Timed annotation = definitionMethod.getAnnotation(Timed.class);

    if (annotation != null) {
      final String givenName = annotation.name();
      if ("".equalsIgnoreCase(givenName)) {
        nameToReturn = definitionMethod.getName();
      } else {
        nameToReturn = givenName;
      }
      return nameToReturn;
    } else {
      return null;
    }
  }

  /**
   *
   * @param method
   *          method
   * @return String
   */
  public static String getCommandGroupName(final ResourceMethod method) {
    final Method definitionMethod = method.getInvocable().getDefinitionMethod();
    return definitionMethod.getDeclaringClass().getName();
  }
}
