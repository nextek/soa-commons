package com.sample.soa.hystrix;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 */
@Component()
public class SOAHystrixStreamLifeCycle implements ApplicationListener<ContextClosedEvent> {

  /**
   *
   * @param event ContextClosedEvent
   */
  @Override()
  public void onApplicationEvent(final ContextClosedEvent event) {
    HystrixMetricsStreamServlet.shutdown();
  }
}
