package com.sample.soa.hystrix;

import java.io.IOException;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;

import org.apache.log4j.MDC;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 */
public class SOAHystrixClientFilter implements ClientRequestFilter {

  /**
   * This method ensures the message ID from the header is passed down to
   * the called applications.
   *
   * @param requestContext ClientRequestContext
   *
   */
  @Override()
  public void filter(final ClientRequestContext requestContext) throws IOException {
    requestContext.getHeaders().add("hltMessageId", MDC.get("message_id"));
  }
}
