package com.sample.soa.errors.exceptions;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class GatewayTimeoutException extends ServerErrorException {

  /**
   *
   */
  private static final long serialVersionUID = -5647577212982828650L;

  /**
   * Default constructor.
   */
  public GatewayTimeoutException() {
    super("Gateway Timeout", Status.GATEWAY_TIMEOUT);
  }

}
