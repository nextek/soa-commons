package com.sample.soa.errors.exceptions;

import com.sample.soa.errors.constants.ErrorCode;
import com.sample.soa.errors.constants.ErrorType;

/**
 * Conversion specific exception handler.
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class ConversionException extends AppException {

  /**
   *
   */
  private static final long serialVersionUID = 6096876271673733433L;

  /**
   * @param ex Exception
   */
  public ConversionException(final Exception ex) {
    super(ErrorType.CONVERSION_ERROR,
        ErrorCode.getCode(ErrorType.CONVERSION_ERROR).getErrCode(),
        ErrorCode.getCode(ErrorType.CONVERSION_ERROR).getMessage());
  }
}
