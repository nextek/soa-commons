/** Provides common error exception handlers. */
/**
 * SOA Common error handling Guide.
 *
 * The following error code ranges should be used for the listed examples.
 *
 * - Well defined HTTP responses: Use the HTTP Status as the error code
 *   * Example 1: Bad request             - error code 400
 *   * Example 2: Resource already exists - error code 409
 *
 *   * Note: BadGatewayException provides a secondaryErrCode to pass the error code
 *   *       from the externally-called application back through the service
 *
 * - Application Defined Errors use any value greater than 999 and less than 9999
 *   * Example: Service needs to log an error that an action exceeded timeout,
 *              developer throws APP_DEFINED_ERROR(1000, "Timeout: %s", HttpStatus.GATEWAY_TIMEOUT)
 *
 * - UnHandled exceptions should use 9999 - UNHANDLED_EXCEPTION
 *   * Example:
 *         try {
 *         } catch (KnownExceptionType) {
 *         } catch {
 *           throw new UnHandledException(...);  // should include stack trace
 *         }
 */
package com.sample.soa.errors.exceptions;
