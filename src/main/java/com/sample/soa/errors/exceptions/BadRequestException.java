package com.sample.soa.errors.exceptions;

import java.util.List;

import com.sample.soa.errors.constants.ErrorCode;
import com.sample.soa.errors.constants.ErrorType;
import com.sample.soa.errors.response.Notification;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class BadRequestException extends AppException {
  /**
   *
   */
  private static final long serialVersionUID = 4947915695622326212L;

  /**
   * Default Constructor.
   */
  public BadRequestException() {
    super(ErrorType.BAD_REQUEST,
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getErrCode(),
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getMessage());
  }

  /**
   *
   * @param notificationList List
   */
  public BadRequestException(final List<Notification> notificationList) {
    super(ErrorType.BAD_REQUEST,
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getErrCode(),
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getMessage(),
        notificationList);
  }

  /**
   *
   * @param message String
   */
  public BadRequestException(final String message) {
    super(ErrorType.BAD_REQUEST,
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getErrCode(),
        message);
  }

  /**
   *
   * @param message String
   * @param notificationList List
   */
  public BadRequestException(final String message, final List<Notification> notificationList) {
    super(ErrorType.BAD_REQUEST,
        ErrorCode.getCode(ErrorType.BAD_REQUEST).getErrCode(),
        message,
        notificationList);
  }
}
