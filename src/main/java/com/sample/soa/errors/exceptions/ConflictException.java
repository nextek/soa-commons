package com.sample.soa.errors.exceptions;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response.Status;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class ConflictException extends ClientErrorException {

  /**
   *
   */
  private static final long serialVersionUID = -5647577212982828650L;

  /**
   * Default constructor.
   */
  public ConflictException() {
    super("Conflict", Status.CONFLICT);
  }

}
