package com.sample.soa.errors.exceptions;

import java.util.List;

import com.sample.soa.config.ErrorContext;
import com.sample.soa.errors.constants.ErrorMeta;
import com.sample.soa.errors.response.Notification;

// Only use constructors without message arguments when the message doesn't include
// substitution, otherwise include constructors that require a substitution argument.

/**
 * General Application Exception base class.
 *
 * @author chuck
 * @version 1
 */
public abstract class AppException extends RuntimeException {
  /**
   *
   */
  private static final long serialVersionUID = -1821594290613366372L;
  private String appErrorType;
  private int code;
  private String errorContext=ErrorContext.getContext();
  private String message;
  private List<Notification> notificationList;

  /**
   *
   * @param appError ErrorMeta
   */
  public AppException(final ErrorMeta appError) {
    if (appError != null) {
      this.code = appError.getErrCode();
      this.message = appError.getMessage();
    }
  }

  /**
   *
   * @param appError ErrorMeta
   * @param notificationList List
   */
  public AppException(final ErrorMeta appError, final List<Notification> notificationList) {
    if (appError != null) {
      this.code = appError.getErrCode();
      this.message = appError.getMessage();
    }
    setNotificationList(notificationList);
  }

  /**
   *
   * @param appErrorType ErrorMeta
   * @param code int
   * @param message String
   * @param notificationList List
   */
  public AppException(final String appErrorType, final int code, final String message,
      final List<Notification> notificationList) {
    super(message);
    this.appErrorType = appErrorType;
    this.code = code;
    this.message = message;
    setNotificationList(notificationList);
  }

  /**
   *
   * @param appErrorType ErrorMeta
   * @param code int
   * @param message String
   */
  public AppException(final String appErrorType, final int code, final String message) {
    super(message);
    this.appErrorType = appErrorType;
    this.code = code;
    this.message = message;
  }

  /**
   *
   * @return int
   */
  public int getCode() {
    return code;
  }

  /**
   *
   * @return String
   */
  public String getAppContext() {
    return errorContext;
  }

  @Override()
  public String getMessage() {
    return message;
  }

  /**
   *
   * @return List
   */
  public List<Notification> getNotificationList() {
    return notificationList;
  }

  /**
   *
   * @return String
   */
  public String getAppErrorType() {
    return appErrorType;
  }

  /**
   *
   * @param notificationList List
   */
  public void setNotificationList(final List<Notification> notificationList) {
    this.notificationList = notificationList;
  }
}
