package com.sample.soa.errors.constants;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class NotificationTypeMeta {
  private int errCode;
  private String message;

  /**
   *
   */
  public NotificationTypeMeta() {
  }

  /**
   *
   * @param errCode int
   * @param message String
   */
  public NotificationTypeMeta(final int errCode, final String message) {
    this.errCode = errCode;
    this.message = message;
  }

  /**
   *
   * @return String
   */
  public String getErrCode() {
    return Integer.toString(errCode);
  }

  /**
   *
   * @param errCode int
   */
  public void setErrCode(final int errCode) {
    this.errCode = errCode;
  }

  /**
   *
   * @return String
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param message String
   */
  public void setMessage(final String message) {
    this.message = message;
  }
}
