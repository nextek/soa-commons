package com.sample.soa.errors.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a constraint notification type. The ordinal values will be used for notification
 * codes.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public final class NotificationTypes {

  private static final int UNKNOWN_CODE = 999;
  private static final String DEFAULT_CODE = "unknown";
  private static final int MIN_NEW_NOTIFICATION_CODE = 500;
  private static final String NOT_NULL = "NotNull";
  private static int nextNotificationCode = MIN_NEW_NOTIFICATION_CODE;
  private static int annotationCode = 1;
  private static Map<String, NotificationTypeMeta> notificationTypes = new HashMap<>();

  private NotificationTypes() {
  }

  static {
    notificationTypes.put("AssertFalse",
        new NotificationTypeMeta(annotationCode++, "Value must be false."));
    notificationTypes.put("AssertFalse.List",
        new NotificationTypeMeta(annotationCode++, "Value must be false (List)."));
    notificationTypes.put("AssertTrue",
        new NotificationTypeMeta(annotationCode++, "Value must be true."));
    notificationTypes.put("AssertTrue.List",
        new NotificationTypeMeta(annotationCode++, "Value must be true (List)."));
    notificationTypes.put("DecimalMax",
        new NotificationTypeMeta(annotationCode++,
            "Decimal number must be lower or equal to the specified maximum."));
    notificationTypes.put("DecimalMax.List",
        new NotificationTypeMeta(annotationCode++,
            "Decimal number must be lower or equal to the specified maximum (List)."));
    notificationTypes.put("DecimalMin",
        new NotificationTypeMeta(annotationCode++,
            "Decimal number must be higher or equal to the specified minimum."));
    notificationTypes.put("DecimalMin.List",
        new NotificationTypeMeta(annotationCode++,
            "Decimal number must be higher or equal to the specified minimum (List)."));
    notificationTypes.put("Digits",
        new NotificationTypeMeta(annotationCode++,
            "Number cannot exceed the specified number of digits."));
    notificationTypes.put("Digits.List",
        new NotificationTypeMeta(annotationCode++,
            "Number cannot exceed the specified number of digits (List)."));
    notificationTypes.put("Future",
        new NotificationTypeMeta(annotationCode++, "Date must be in the future."));
    notificationTypes.put("Max",
        new NotificationTypeMeta(annotationCode++,
            "Number must be lower or equal to the specified maximum."));
    notificationTypes.put("Max.List",
        new NotificationTypeMeta(annotationCode++,
            "Number must be lower or equal to the specified maximum (List)."));
    notificationTypes.put("Min",
        new NotificationTypeMeta(annotationCode++,
            "Number must be higher or equal to the specified minimum."));
    notificationTypes.put("Min.List",
        new NotificationTypeMeta(annotationCode++,
            "Number must be higher or equal to the specified minimum (List)."));
    notificationTypes.put(NOT_NULL,
        new NotificationTypeMeta(annotationCode++, "Value is required."));
    notificationTypes.put("NotEmpty", notificationTypes.get(NOT_NULL));
    notificationTypes.put("NotNull.List",
        new NotificationTypeMeta(annotationCode++, "Value is required (List)."));
    notificationTypes.put("Null",
        new NotificationTypeMeta(annotationCode++, "Value must be null."));
    notificationTypes.put("Null.List",
        new NotificationTypeMeta(annotationCode++, "Value must be null (List)."));
    notificationTypes.put("Past",
        new NotificationTypeMeta(annotationCode++, "Date must be in the past."));
    notificationTypes.put("Past.List",
        new NotificationTypeMeta(annotationCode++, "Date must be in the past (List)."));
    notificationTypes.put("Pattern",
        new NotificationTypeMeta(annotationCode++,
            "String must match the specified regular expression."));
    notificationTypes.put("Pattern.List",
        new NotificationTypeMeta(annotationCode++,
            "String must match one of the specified regular expressions."));
    notificationTypes.put("Size",
        new NotificationTypeMeta(annotationCode++,
            "The number of elements in the collection must be between the boundaries."));
    notificationTypes.put("Size.List",
        new NotificationTypeMeta(annotationCode++,
            "The number of elements in the collection must be between the boundaries (List)."));
    notificationTypes.put("AssertDateTime",
        new NotificationTypeMeta(annotationCode++,
            "The DateTime must be valid and in the format of yyyy-MM-ddTHH:mm:ss+xx:xx."));
    notificationTypes.put("AssertDate",
        new NotificationTypeMeta(annotationCode++,
            "The Date must be valid and in the format of yyyy-MM-dd."));
    notificationTypes.put("AssertTime",
        new NotificationTypeMeta(annotationCode++,
            "The Time must be valid and in the format of 'HH:mm:ss'."));
    notificationTypes.put("AssertDate.List",
        new NotificationTypeMeta(annotationCode++,
            "The Date must be valid and in the format of yyyy-MM-dd (List)."));
    notificationTypes.put("AssertDateTime.List",
        new NotificationTypeMeta(annotationCode++,
            "The DateTime must be valid and in the format of yyyy-MM-ddTHH:mm:ss+xx:xx (List)."));
    notificationTypes.put("JsonParam",
        new NotificationTypeMeta(annotationCode++, "Invalid JSON Parameter Value"));
    notificationTypes.put("JsonParse",
        new NotificationTypeMeta(annotationCode++, "JSON Parse Error"));
    notificationTypes.put("QueryParam",
        new NotificationTypeMeta(annotationCode++, "Invalid Query Parameter Value"));
    notificationTypes.put("AssertValidEmail",
        new NotificationTypeMeta(annotationCode++, "Invalid email address."));
    notificationTypes.put(DEFAULT_CODE, new NotificationTypeMeta(UNKNOWN_CODE, null));
  }

  /**
   *
   * @param key String
   * @return NotificationTypeMeta
   */
  public static NotificationTypeMeta getCode(final String key) {
    if (notificationTypes.containsKey(key)) {
      return notificationTypes.get(key);
    }

    return notificationTypes.get(DEFAULT_CODE);
  }

  /**
   *
   * @param key String
   * @param message String
   * @return int
   * @throws Exception exception
   */
  public static int addCode(final String key, final String message) throws Exception {
    if (notificationTypes.containsKey(key)) {
      throw new Exception("Unable to add notification type. Notification type already exists.");
    }

    final int code;
    code = nextNotificationCode++;

    notificationTypes.put(key, new NotificationTypeMeta(code, message));

    return code;
  }
}
