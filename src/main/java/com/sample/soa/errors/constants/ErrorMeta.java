package com.sample.soa.errors.constants;

/**
 * @author chuck
 * @version 1
 *
 */
public class ErrorMeta {
  private int errCode;
  private String message;

  /**
   *
   */
  public ErrorMeta() {
  }

  /**
   * @param errCode int
   * @param message String
   */
  public ErrorMeta(final int errCode, final String message) {
    this.errCode = errCode;
    this.message = message;
  }

  /**
   * @return int
   */
  public int getErrCode() {
    return errCode;
  }

  /**
   *
   * @param errCode int
   */
  public void setErrCode(final int errCode) {
    this.errCode = errCode;
  }

  /**
   *
   * @return String
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param messageFmt String
   */
  public void setMessage(final String messageFmt) {
    this.message = messageFmt;
  }
}
