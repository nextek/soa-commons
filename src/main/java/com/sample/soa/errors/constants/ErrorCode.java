package com.sample.soa.errors.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a error code type. Used by the AppException class and subclasses.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public final class ErrorCode {
  private static final int APP_LOWER_LIMIT_CODE = 1000;
  private static Map<String, ErrorMeta> errors = new HashMap<>();
  private static int errorCode = APP_LOWER_LIMIT_CODE - 1;

  private ErrorCode() {
  }

  static {
    errors.put(ErrorType.CONSTRAINT_VIOLATION, new ErrorMeta(errorCode--, "Constraint Violation"));
    errors.put(ErrorType.INVALID_PARAMETER, new ErrorMeta(errorCode--, "Invalid Parameter Value"));
    errors.put(ErrorType.BAD_REQUEST, new ErrorMeta(errorCode--, "Bad Request"));
    errors.put(ErrorType.CONVERSION_ERROR, new ErrorMeta(errorCode--, "Conversion Error"));
    errors.put(ErrorType.INTERNAL_SERVER_ERROR, new ErrorMeta(errorCode--,
        "Internal Server Error"));
  }

  /**
   * @param key String
   * @return ErrorMeta
   */
  public static ErrorMeta getCode(final String key) {
    if (errors.containsKey(key)) {
      return errors.get(key);
    }

    return errors.get("unknown");
  }

  /**
   * @param key String
   * @param code int
   * @param message String
   * @return Map
   * @throws Exception exception
   */
  public static Map<String, ErrorMeta> addCode(final String key, final Integer code,
      final String message) throws Exception {
    if (code >= APP_LOWER_LIMIT_CODE) {
      errors.put(key, new ErrorMeta(code, message));
    } else {
      throw new Exception("Unable to add error code. Error code is invalid.");
    }

    return errors;
  }
}
