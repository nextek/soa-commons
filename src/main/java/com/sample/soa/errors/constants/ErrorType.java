package com.sample.soa.errors.constants;

/**
 * Common ErrorDetail Code List.
 * @author chuck
 * @version 1
 */
public final class ErrorType {

  /**
  *
  */
  public static final String CONSTRAINT_VIOLATION = "constraintViolation";
  /**
  *
  */
  public static final String INVALID_PARAMETER = "invalidParameter";
  /**
   *
   */
  public static final String BAD_REQUEST = "badRequest";
  /**
   *
   */
  public static final String CONVERSION_ERROR = "conversionError";
  /**
   *
   */
  public static final String INTERNAL_SERVER_ERROR = "internalServerError";

  private ErrorType() {
  }

}
