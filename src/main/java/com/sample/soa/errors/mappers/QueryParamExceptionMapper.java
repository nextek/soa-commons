package com.sample.soa.errors.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.glassfish.jersey.server.ParamException.QueryParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.soa.config.ErrorContext;
import com.sample.soa.errors.constants.ErrorCode;
import com.sample.soa.errors.constants.ErrorType;
import com.sample.soa.errors.response.ErrorDetail;
import com.sample.soa.errors.response.Notification;
import com.sample.soa.errors.response.NotificationList;

/**
 * Responsible for mapping a ParamException to a response.
 *
 * @author Chuck Ludwigsen
 * @version 1.1
 */
public class QueryParamExceptionMapper implements ExceptionMapper<QueryParamException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(QueryParamExceptionMapper.class);

  @Override()
  public Response toResponse(final QueryParamException exception) {
    LOGGER.error("Query parameter exception occurred", exception);
    final ErrorDetail errorDetail = new ErrorDetail();
    final NotificationList notificationList = new NotificationList();

    final Notification notification = new Notification("QueryParam");
    notification.getFields().add(exception.getParameterName());
    notificationList.add(notification);

    errorDetail.setContext(ErrorContext.getContext());
    errorDetail.setCode(ErrorCode.getCode(ErrorType.INVALID_PARAMETER).getErrCode());
    errorDetail.setMessage(ErrorCode.getCode(ErrorType.INVALID_PARAMETER).getMessage());
    errorDetail.setNotifications(notificationList.get());

    return Response.status(Response.Status.BAD_REQUEST).entity(errorDetail).build();
  }
}
