package com.sample.soa.errors.mappers;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.soa.config.ErrorContext;
import com.sample.soa.errors.constants.ErrorCode;
import com.sample.soa.errors.constants.ErrorType;
import com.sample.soa.errors.response.ErrorDetail;
import com.sample.soa.errors.response.Notification;
import com.sample.soa.errors.response.NotificationList;

/**
 * Responsible for mapping a ConstraintViolationException to a response.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public class ConstraintViolationExceptionMapper implements
    ExceptionMapper<ConstraintViolationException> {
  private static final int TOKEN_NAME_OFFSET = 2;
  private static final String DOT_DELIMETER = ".";
  private static final Logger LOGGER = LoggerFactory
      .getLogger(ConstraintViolationExceptionMapper.class);

  @Override()
  public Response toResponse(final ConstraintViolationException exception) {
    final ErrorDetail error = new ErrorDetail();
    final NotificationList notificationList = new NotificationList();

    LOGGER.debug("Constraint violation exception.", exception);

    error.setContext(ErrorContext.getContext());
    error.setCode(ErrorCode.getCode(ErrorType.CONSTRAINT_VIOLATION).getErrCode());
    error.setMessage(ErrorCode.getCode(ErrorType.CONSTRAINT_VIOLATION).getMessage());

    for (final ConstraintViolation<?> constraintViolation : exception.getConstraintViolations()) {
      final List<String> pathNodes = new ArrayList<>();
      for(javax.validation.Path.Node node : constraintViolation.getPropertyPath()) {
        pathNodes.add(node.getName());
      }
      final String pathString = pathNodesToString(pathNodes);

      final String annotationTypeNameQualified = constraintViolation.getConstraintDescriptor()
          .getAnnotation().annotationType().getName();
      final String annotationTypeName = getAnnotationTypeName(annotationTypeNameQualified);

      final Notification notification = new Notification(annotationTypeName);
      notification.getFields().add(pathString);

      notificationList.add(notification);
    }

    error.setNotifications(notificationList.get());

    return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
  }

  private String getAnnotationTypeName(final String annotationTypeName) {
    final String[] annotationTypeNameTokens = annotationTypeName.split("\\.");
    final int lengthOfTokens = annotationTypeNameTokens.length;
    String annotationName = null;

    if (annotationTypeName.contains("List")) {
      annotationName = annotationTypeNameTokens[lengthOfTokens - TOKEN_NAME_OFFSET] + DOT_DELIMETER
                       + annotationTypeNameTokens[lengthOfTokens - 1];
    } else {
      annotationName = annotationTypeNameTokens[lengthOfTokens - 1];
    }

    return annotationName;
  }

  private static String pathNodesToString(final List<String> pathNodes) {
    final StringJoiner refName = new StringJoiner(DOT_DELIMETER);
    for(String pathNode : pathNodes) {
      if (pathNode != null && pathNode.length() > 0) {
        refName.add(pathNode);
      }
    }

    if(refName.length() == 0) {
      refName.add("unknown");
    }
    return refName.toString();
  }
}
