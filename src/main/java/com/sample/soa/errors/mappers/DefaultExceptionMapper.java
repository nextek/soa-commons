package com.sample.soa.errors.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chuck
 * @version 1
 *
 */
@Provider()
public class DefaultExceptionMapper implements ExceptionMapper<Throwable> {
  private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExceptionMapper.class);

  /**
   * Default constructor.
   */
  public DefaultExceptionMapper() {
  }

  @Override()
  public Response toResponse(final Throwable exception) {
    LOGGER.error("internal server error occurred", exception);

    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("").build();
  }
}
