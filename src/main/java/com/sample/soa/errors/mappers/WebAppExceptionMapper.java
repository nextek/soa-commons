package com.sample.soa.errors.mappers;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chuck
 * @version 1
 */
@Provider()
public class WebAppExceptionMapper implements ExceptionMapper<WebApplicationException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(WebAppExceptionMapper.class);

  /**
   * Default constructor.
   */
  public WebAppExceptionMapper() {
  }

  @Override()
  public Response toResponse(final WebApplicationException exception) {
    LOGGER.error("Web Application Exception occurred", exception);

    return Response.status(exception.getResponse().getStatusInfo()).entity("").build();
  }
}
