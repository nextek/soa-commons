package com.sample.soa.errors.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.sample.soa.config.ErrorContext;
import com.sample.soa.errors.constants.ErrorCode;
import com.sample.soa.errors.constants.ErrorType;
import com.sample.soa.errors.response.ErrorDetail;
import com.sample.soa.errors.response.Notification;
import com.sample.soa.errors.response.NotificationList;

/**
 * Responsible for mapping a ParamException to a response.
 *
 * @author Chuck Ludwigsen
 * @version 1.0
 */
public class JsonParseExceptionMapper implements ExceptionMapper<JsonParseException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonParseExceptionMapper.class);

  @Override()
  public Response toResponse(final JsonParseException exception) {
    LOGGER.error("JSON parsing exception occurred", exception);

    final ErrorDetail errorDetail = new ErrorDetail();
    final NotificationList notificationList = new NotificationList();

    final Notification notification = new Notification("JsonParse");
    final JsonLocation location = exception.getLocation();
    final String fieldText = "Line:" + location.getLineNr() + ";Column:" + location.getColumnNr();
    notification.getFields().add(fieldText);
    notificationList.add(notification);

    errorDetail.setContext(ErrorContext.getContext());
    errorDetail.setCode(ErrorCode.getCode(ErrorType.INVALID_PARAMETER).getErrCode());
    errorDetail.setMessage(ErrorCode.getCode(ErrorType.INVALID_PARAMETER).getMessage());
    errorDetail.setNotifications(notificationList.get());

    return Response.status(Response.Status.BAD_REQUEST).entity(errorDetail).build();
  }
}
