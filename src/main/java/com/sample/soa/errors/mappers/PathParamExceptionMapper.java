package com.sample.soa.errors.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.server.ParamException.PathParamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chuck
 * @version 1
 */
@Provider()
public class PathParamExceptionMapper implements ExceptionMapper<PathParamException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(PathParamExceptionMapper.class);

  /**
   * Default constructor.
   */
  public PathParamExceptionMapper() {
  }

  @Override()
  public Response toResponse(final PathParamException exception) {
    LOGGER.error("Path parameter exception occurred", exception);

    return Response.status(exception.getResponse().getStatusInfo()).entity("").build();
  }
}
