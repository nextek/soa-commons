package com.sample.soa.errors.mappers;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.soa.config.ErrorContext;
import com.sample.soa.errors.exceptions.AppException;
import com.sample.soa.errors.response.ErrorDetail;

/**
 * @author chuck
 * @version 1
 *
 */
@Provider()
public class AppExceptionMapper implements ExceptionMapper<AppException> {
  private static final Logger LOGGER = LoggerFactory.getLogger(AppExceptionMapper.class);

  /**
   * Default constructor.
   */
  public AppExceptionMapper() {
  }

  @Override()
  public Response toResponse(final AppException exception) {
    LOGGER.error("application exception occurred", exception);

    final ErrorDetail errorDetail = new ErrorDetail();

    errorDetail.setContext(ErrorContext.getContext());
    errorDetail.setCode(exception.getCode());
    errorDetail.setMessage(exception.getMessage());

    if (exception.getNotificationList() != null && !exception.getNotificationList().isEmpty()) {
      errorDetail.setNotifications(exception.getNotificationList());
    }

    return Response
        .status(Response.Status.BAD_REQUEST)
        .entity(errorDetail)
        .build();
  }
}
