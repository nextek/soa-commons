package com.sample.soa.errors.response;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class ErrorDetail {
  private String context;
  private int code;
  private String message;
  private List<Notification> notifications = new ArrayList<>();

  /**
   *
   */
  public ErrorDetail() {
  }

  /**
   *
   * @param context String
   * @param code int
   * @param message String
   * @param notifications List
   */
  public ErrorDetail(final String context, final int code, final String message,
      final List<Notification> notifications) {
    this.context = context;
    this.code = code;
    this.message = message;
    this.notifications = notifications;
  }

  /**
   *
   * @return String
   */
  public String getContext() {
    return context;
  }

  /**
   *
   * @param context String
   */
  public void setContext(final String context) {
    this.context = context;
  }

  /**
   *
   * @return int
   */
  public int getCode() {
    return code;
  }

  /**
   *
   * @param code int
   */
  public void setCode(final int code) {
    this.code = code;
  }

  /**
   *
   * @return String
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param message String
   */
  public void setMessage(final String message) {
    this.message = message;
  }

  /**
   *
   * @return List
   */
  public List<Notification> getNotifications() {
    return notifications;
  }

  /**
   *
   * @param notifications List
   */
  public void setNotifications(final List<Notification> notifications) {
    this.notifications = notifications;
  }
}
