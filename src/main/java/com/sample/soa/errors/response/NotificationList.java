/**
 *
 */
package com.sample.soa.errors.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class NotificationList extends ArrayList<Notification> {

  /**
   *
   */
  private static final long serialVersionUID = -6612765111614687928L;

  /**
   * Resulting Notification Array List for all grouped codes.
   */
  private List<Notification> notificationList = new ArrayList<>();

  /**
   * HashMap to construct groupings of Notification codes.
   */
  private Map<String, Notification> notifications = new HashMap<>();

  /**
   *
   */
  public NotificationList() {
  }

  /**
   * @return the notificationList
   */
  public final List<Notification> get() {
    notificationList.clear();
    notificationList.addAll(notifications.values());
    return notificationList;
  }

  /**
   * @return the notifications
   */
  public final Map<String, Notification> getNotifications() {
    return notifications;
  }

  /**
   * @param notification the notification to add to the map
   * @return boolean
   */
  @Override()
  public final boolean add(final Notification notification) {
    final boolean returnOk = true;

    if (!notifications.containsKey(notification.getCode())) {
      notifications.put(notification.getCode(), notification);
    } else {
      if (notification.getFields() != null && !notification.getFields().isEmpty()) {
        notifications.get(notification.getCode()).getFields().addAll(notification.getFields());
      }
    }

    return returnOk;
  }

}
