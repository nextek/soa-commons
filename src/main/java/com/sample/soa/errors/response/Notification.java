package com.sample.soa.errors.response;

import java.util.ArrayList;
import java.util.List;

import com.sample.soa.errors.constants.NotificationTypeMeta;
import com.sample.soa.errors.constants.NotificationTypes;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class Notification {
  private String code;
  private List<String> fields = new ArrayList<>();
  private String message;

  /**
   *
   */
  public Notification() {
  }

  /**
   *
   * @param key String
   */
  public Notification(final String key) {
    final NotificationTypeMeta notificationType = NotificationTypes.getCode(key);
    if (notificationType.getErrCode() != null) {
      this.code = notificationType.getErrCode();
      this.message = notificationType.getMessage();
    }
  }

  /**
   *
   * @return String
   */
  public String getCode() {
    return code;
  }

  /**
   *
   * @param code String
   */
  public void setCode(final String code) {
    this.code = code;
  }

  /**
   *
   * @return List
   */
  public List<String> getFields() {
    return fields;
  }

  /**
   *
   * @param fields List
   */
  public void setFields(final List<String> fields) {
    this.fields = fields;
  }

  /**
   *
   * @return String
   */
  public String getMessage() {
    return message;
  }

  /**
   *
   * @param message String
   */
  public void setMessage(final String message) {
    this.message = message;
  }
}
