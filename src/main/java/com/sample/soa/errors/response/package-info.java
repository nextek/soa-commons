/** Response class to encapsulate elements for HTTP Response. */
/**
 * SOA Common error handling Guide.
 *
 * The ErrorDetail response class encapsulates the relevant information for the exception into
 * a class object to inject into the jersey response.
 *
 * AppException:
 *   Context
 *   ErrorDetail Code (number)
 *   Message
 *
 * BadGateway:
 *   Context
 *   ErrorDetail Code (number)
 *   Message
 *   Secondary error code from external application
 */
package com.sample.soa.errors.response;
