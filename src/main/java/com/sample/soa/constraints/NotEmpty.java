package com.sample.soa.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
@Target(value={ ElementType.FIELD, ElementType.METHOD,
    ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(value=RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEmptyValidator.class)
@Documented()
public @interface NotEmpty {

  /**
   * Message assigned to annotation.
   *
   * @return A message
   */
  String message() default "Value is required.";

  /**
   * Assigned groups.
   *
   * @return An array of classes.
   */
  Class<?>[] groups() default {};

  /**
   * Assigned payload.
   *
   * @return An array of classes that extend Payload.
   */
  Class<? extends Payload>[] payload() default {};
}
