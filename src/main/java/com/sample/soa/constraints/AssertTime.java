package com.sample.soa.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Used to assert a String value using the format of 'HH:mm:ss+00:00' or 'HH:mm:ssZ'.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
@Target(value = {ElementType.FIELD, ElementType.METHOD,
    ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AssertTimeValidator.class)
@Documented()
public @interface AssertTime {
  /**
   * Message assigned to annotation.
   *
   * @return A message
   */
  String message() default "Invalid time.";

  /**
   * Assigned groups.
   *
   * @return An array of classes.
   */
  Class<?>[] groups() default {};

  /**
   * Assigned payload.
   *
   * @return An array of classes that extend Payload.
   */
  Class<? extends Payload>[] payload() default {};
}
