package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * The annotated element must be in a valid email format.
 * Supported types are String. <pre>null</pre> elements are considered valid
 *
 * @author Brandon Schendel
 * @version 1.0
 */
public class AssertEmailValidator implements ConstraintValidator<AssertValidEmail, Object> {

  @Override()
  public void initialize(final AssertValidEmail assertValidEmail) {
  }

  @Override()
  public boolean isValid(final Object value,
      final ConstraintValidatorContext constraintValidatorContext) {

    Collection<String> emailCollection;

    if (value instanceof String[]) {
      emailCollection = Arrays.asList((String[]) value);
    } else if (value instanceof Set) {
      emailCollection = (Set<String>) value;
    } else if (value instanceof List) {
      emailCollection = (List<String>) value;
    } else if (value instanceof String) {
      emailCollection = Collections.singleton((String) value);
    } else {
      emailCollection = Collections.emptyList();
    }

    return emailCollection.stream().allMatch(this::isEmailValid);
  }

  private boolean isEmailValid(final String email) {
    if (email == null || "".equals(email)) {
      return false;
    }

    final String emailTrimmed = email.trim();

    final EmailValidator ev = EmailValidator.getInstance();
    return ev.isValid(emailTrimmed);
  }
}
