package com.sample.soa.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Brandon Schendel
 * @version 1
 *
 */
@Target(value={ ElementType.FIELD, ElementType.METHOD,
    ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(value=RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AssertEmailValidator.class)
@Documented()
public @interface AssertValidEmail {

  /**
   *
   * @return String
   */
  String message() default "Invalid email.";

  /**
   *
   * @return Array
   */
  Class<?>[] groups() default { };

  /**
   *
   * @return Array
   */
  Class<? extends Payload>[] payload() default { };

  /**
   * Defines several @AssertValidEmail annotations on the same element.
   *
   * @author Brandon Schendel
   */
  @Target(value={ ElementType.FIELD, ElementType.METHOD,
      ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
  @Retention(value=RetentionPolicy.RUNTIME)
  @Documented()
  @interface List {
    /**
     *
     * @return List
     */
    AssertValidEmail[] value();
  }

}
