/**
 * Contains all the custom provided constraints used to validate Java bean fields.
 */
package com.sample.soa.constraints;
