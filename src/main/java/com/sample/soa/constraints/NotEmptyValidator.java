/**
 *
 */
package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * The annotated field or collection must be a String, array of String, or String collection.
 *
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class NotEmptyValidator implements ConstraintValidator<NotEmpty, Object> {

  @Override()
  public void initialize(final NotEmpty notEmpty) {
  }

  @Override()
  public boolean isValid(final Object value,
      final ConstraintValidatorContext constraintValidatorContext) {

    Collection<String> stringCollection;

    if (value instanceof String[]) {
      stringCollection = Arrays.asList((String[]) value);
    } else if (value instanceof Set) {
      stringCollection = (Set<String>) value;
    } else if (value instanceof List) {
      stringCollection = (List<String>) value;
    } else if (value instanceof String) {
      stringCollection = Collections.singleton((String) value);
    } else {
      stringCollection = Collections.emptyList();
    }

    if (stringCollection.isEmpty()) {
      return false;
    } else {
      return stringCollection.stream().allMatch(this::isNullOrEmpty);
    }
  }

  private boolean isNullOrEmpty(final String value) {
    boolean isValid = true;

    if (value == null || value.isEmpty()) {
      isValid = false;
    }

    return isValid;
  }
}
