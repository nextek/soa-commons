package com.sample.soa.constraints;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author Sean Alphonse
 * @version 1
 *
 */
@Target(value={ ElementType.FIELD, ElementType.METHOD,
    ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
@Retention(value=RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AssertDateValidator.class)
@Documented()
public @interface AssertDate {

  /**
   *
   * @return String
   */
  String message() default "Invalid date.";

  /**
   *
   * @return Array
   */
  Class<?>[] groups() default { };

  /**
   *
   * @return Array
   */
  Class<? extends Payload>[] payload() default { };

  /**
   * Defines several @AssertDate annotations on the same element.
   *
   * @author Chuck Ludwigsen
   */
  @Target(value={ ElementType.FIELD, ElementType.METHOD,
      ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })
  @Retention(value=RetentionPolicy.RUNTIME)
  @Documented()
  @interface List {
    /**
     *
     * @return List
     */
    AssertDate[] value();
  }
}
