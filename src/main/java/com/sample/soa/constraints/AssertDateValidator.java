package com.sample.soa.constraints;


import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * The annotated element must be in the format of yyyy-MM-dd. Supported types are String.
 * <pre>null</pre> elements are considered valid
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public class AssertDateValidator implements ConstraintValidator<AssertDate, Object> {

  @Override()
  public void initialize(final AssertDate assertDate) {
  }

  @Override()
  public boolean isValid(final Object value,
      final ConstraintValidatorContext constraintValidatorContext) {

    Collection<String> dateCollection;

    if (value instanceof String[]) {
      dateCollection = Arrays.asList((String[]) value);
    } else if (value instanceof Set) {
      dateCollection = (Set<String>) value;
    } else if (value instanceof List) {
      dateCollection = (List<String>) value;
    } else if (value instanceof String) {
      dateCollection = Collections.singleton((String) value);
    } else {
      dateCollection = Collections.emptyList();
    }

    return dateCollection.stream().allMatch(this::isDateValid);
  }

  private boolean isDateValid(final String dateString) {
    boolean isValid = true;

    if (dateString != null) {
      try {
        LocalDate.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE);
      } catch (final DateTimeException ex) {
        isValid = false;
      }
    }

    return isValid;
  }
}
