package com.sample.soa.context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * This class represents the context for a transaction.
 *
 * @author Brandon Schendel
 * @version 1.0
 */
public class TxnContext {
  private static final String WILDCARD_LANGUAGE = "*";
  private static Locale defaultNoRequestLanguage = Locale.US;
  private static Locale defaultRequestContentLanguage = Locale.US;
  private HttpHeaders requestHeaders;
  private Locale responseContentLanguage;
  private Date lastModified;

  /**
   * Constructor.
   *
   * @param requestHeaders
   *     The request headers for this transaction context.
   */
  public TxnContext(final HttpHeaders requestHeaders) {
    this.requestHeaders = requestHeaders;
  }

  /**
   * Returns the current default language used when no language was requested.
   *
   * @return The current default language used when no language was requested.
   */
  public static Locale getDefaultNoRequestLanguage() {
    return defaultNoRequestLanguage;
  }

  /**
   * Used to set the default language returned when no language is passed in.
   *
   * @param lang The default request language.
   */
  public static void setDefaultNoRequestLanguage(final Locale lang) {
    if(lang == null) {
      throw new RuntimeException("Invalid default language");
    }
    defaultNoRequestLanguage = lang;
  }

  /**
   * Returns the current default language used when no language was requested.
   *
   * @return The current default language used when no language was requested.
   */
  public static Locale getDefaultRequestContentLanguage() {
    return defaultRequestContentLanguage;
  }

  /**
   * Used to set the default language returned when no language is passed in.
   *
   * @param lang The default request language.
   */
  public static void setDefaultRequestContentLanguage(final Locale lang) {
    if(lang == null) {
      throw new RuntimeException("Invalid default content language");
    }
    defaultRequestContentLanguage = lang;
  }

  /**
   * Returns the request headers passed in on the request.
   *
   * @return The request headers passed in on the request.
   */
  public HttpHeaders getRequestHeaders() {
    return requestHeaders;
  }

  /**
   * Returns the most preferred acceptable language given on the request.
   *
   * @return Returns the most preferred acceptable language given on the request.
   *         Returns the default language if no language is passed in.
   */
  public Locale getPreferredLanguage() {
    final List<Locale> orderedLanguages = getAcceptableLanguages();
    return orderedLanguages.get(0);
  }

  /**
   * A convenience method to retrieve all of the acceptable languages passed in on the
   * Accept-Language header.
   * The languages are returned ordered in priority order with the highest priority first.
   * If no languages are passed in, the default language will be returned.
   *
   * @return The list of languages passed in on the Accept-Language header.
   *         If no languages are passed in, the default language will be returned.
   */
  public List<Locale> getAcceptableLanguages() {
    final List<Locale> orderedLanguages = requestHeaders.getAcceptableLanguages();
    final Locale firstLanguage = orderedLanguages.get(0);
    List<Locale> retLanguages;
    if(orderedLanguages.size() == 1 && WILDCARD_LANGUAGE.equals(firstLanguage.getLanguage())) {
      // If they pass in no languages, getAcceptableLanguages returns a single wildcard language.
      // In this case, return the default language.
      retLanguages = new ArrayList<>();
      retLanguages.add(defaultNoRequestLanguage);
    } else {
      retLanguages = orderedLanguages;
    }
    return retLanguages;
  }

  /**
   * A convenience method to retrive all of the media types passed in on the Accept header.
   * The media types are returned ordered in priority order with the highest priority first.
   *
   * @return The list of media types passed in on the Accept header.
   */
  public List<MediaType> getAcceptableMediaTypes() {
    return requestHeaders.getAcceptableMediaTypes();
  }

  /**
   * Returns the request content language.
   *
   * @return Returns the content language on the request.
   *         If the content language is not passed on the request, then the current default
   *         request content language will be returned.
   */
  public Locale getRequestContentLanguage() {
    final Locale requestContentLanguage = requestHeaders.getLanguage();
    Locale retLocale = null;
    if(requestContentLanguage != null) {
      retLocale = requestContentLanguage;
    } else {
      retLocale = defaultRequestContentLanguage;
    }
    return retLocale;
  }

  /**
   * Returns the version that was requested on the highest priority media type
   * on the incoming Accept header.
   *
   * @return The version of the resource requested. Returns null if no version was given.
   */
  public String getPreferredVersion() {
    final List<String> versionsList = getAcceptableVersions();
    String retVersion = null;
    if(versionsList.size() > 0) {
      retVersion = versionsList.get(0);
    }
    return retVersion;
  }

  /**
   * Returns a list of all versions given with the first element in the list being the version
   * on the highest priority mime type, ordered to the version for the lowest priority mime type.
   * If both the mime type and version need to both be inspected, use the static method
   * getRequestVersionOfMediaType.
   *
   * @return A list of versions passed.  All elements of returned list will be non-null.
   */
  public List<String> getAcceptableVersions() {
    // this is guaranteed to return a list of at least one element
    final List<MediaType> mediaTypes = getAcceptableMediaTypes();
    final List<String> retVersions = new ArrayList<>();
    for(MediaType mediaType : mediaTypes) {
      final String version = getRequestVersionOfMediaType(mediaType);
      if(version != null) {
        retVersions.add(version);
      }
    }
    return retVersions;
  }

  /**
   * Returns the version that was requested on the given media type.
   * May be useful if more then one media type is passed in on request and versions are needed.
   *
   * @param mediaType
   *     The given mediaType to find the version on.
   * @return The version on the given media type.  Returns null if no version given.
   */
  public static String getRequestVersionOfMediaType(final MediaType mediaType) {
    final Map<String, String> parameters = mediaType.getParameters();
    return parameters.get("version");
  }

  /**
   * Sets the content language that will be returned in the Content-Language header.
   *
   * @param contentLanguage
   *     The language of the content being returned.
   */
  public void setResponseContentLanguage(final Locale contentLanguage) {
    this.responseContentLanguage = contentLanguage;
  }

  /**
   * Gets the lastModified field for the response entity associated with this txn.
   *
   * @return Returns the lastModified field.
   */
  public Date getLastModified() {
    return new Date(lastModified.getTime());
  }

  /**
   * Sets the lastModified field. This will be returned in the Last-Modified header.
   *
   * @param lastModified The date/time that the response entity was last modified.
   */
  public void setLastModified(final Date lastModified) {
    this.lastModified = new Date(lastModified.getTime());
  }

  /**
   * Returns a response builder updated with any relevant fields set from this context
   * and based on the given ResponseBuilder.
   *
   * @param responseBldr
   *     A ResponseBuilder to start with when adding additional response data.
   * @return The updated ResponseBuilder.
   */
  public ResponseBuilder updateResponse(final ResponseBuilder responseBldr) {
    ResponseBuilder builder = responseBldr;
    if(this.responseContentLanguage != null) {
      builder = responseBldr.language(this.responseContentLanguage);
    }
    if(this.lastModified != null) {
      builder = responseBldr.lastModified(this.lastModified);
    }
    return builder;
  }
}

