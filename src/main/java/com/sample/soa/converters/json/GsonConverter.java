package com.sample.soa.converters.json;

import org.apache.commons.lang3.time.DateFormatUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * JsonConverter implementation based on the GSON library.
 *
 * @author Tony Piazza, Sean Alphonse
 * @version 1.0
 */
public class GsonConverter implements JsonConverter {
  private final Gson gson = new GsonBuilder().setDateFormat(
      DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern()).create();

  /** Deserializes a JSON string into a Java object.
   * @param source JSON string.
   * @param type Class type.
   * @param <T> type of class to return.
   * @return A Java object representing the same information as found in the JSON string.
   */
  @Override()
  public <T> T fromJson(final String source, final Class<T> type) {
    return gson.fromJson(source, type);
  }

  /** Serializes a Java object into a JSON string.
   * @param source A Java object.
   * @param <T> type of class of the source.
   * @return A JSON string representing the data represented in the Java object.
   */
  @Override()
  public <T> String toJson(final T source) {
    return gson.toJson(source);
  }
}
