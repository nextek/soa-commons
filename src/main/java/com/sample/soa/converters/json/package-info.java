/**
 * This package contains utility method implementations to serialize/deserialize Java object
 * to/from JSON.
 */
package com.sample.soa.converters.json;
