package com.sample.soa.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.logging.http.HttpRequestLogging;
import com.sample.servlet.http.BufferedHttpServletRequest;
import com.sample.servlet.http.BufferedHttpServletResponse;
import com.sample.soa.config.LogResourceConfig;
import com.sample.util.RegularExpressionStringFilter;
import com.sample.util.StringFilter;

/**
 *
 * @author Raveesh_Rana
 * @version 1.0
 */
public class LogFilter implements Filter {

  /**
   * This is used to log the request and response.
   */
  private static final Logger ACCESS_LOGGER = LoggerFactory.getLogger("accessLogger");

  /**
   * Root Logger used to print any other message if any.
   */
  private static final Logger LOGGER = LoggerFactory.getLogger(LogFilter.class);

  /**
   * If set true should log the response body. Unless the URI is in filter list.
   */
  private static final boolean LOG_RESPONSE_BODY = LogResourceConfig.ResponseLogConfig.isBody();

  private static final boolean LOG_REQUEST_BODY = LogResourceConfig.RequestLogConfig.isBody();

  private static final Set<String> URI_REQUEST_LOGGING_FILTER = LogResourceConfig.RequestLogConfig
      .getUriFilter();

  private static final Set<String> URI_RESPONSE_LOGGING_FILTER = LogResourceConfig.ResponseLogConfig
      .getUriFilter();

  private static final List<StringFilter> STRING_BODY_FILTERS = new ArrayList<StringFilter>();

  private static final Set<String> EXP_TO_MASK = LogResourceConfig.getMaskPattern();

  private static final String MASK_REPLACEMENT_STRING = "xxxx";

  @Override()
  public void destroy() {
  }

  @Override()
  public void doFilter(final ServletRequest request, final ServletResponse response,
      final FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    HttpServletResponse httpServletResponse = (HttpServletResponse) response;

    /*
     * This will set up the logging if the request body is to be logged.
     */
    if (LOG_REQUEST_BODY && (URI_REQUEST_LOGGING_FILTER == null
        || URI_REQUEST_LOGGING_FILTER.contains(httpServletRequest.getRequestURI()))) {
      try {
        final BufferedHttpServletRequest bufferedHttpServletRequest =
            new BufferedHttpServletRequest(
            httpServletRequest);
        httpServletRequest = bufferedHttpServletRequest;
      } catch (IOException e) {
        LOGGER.error("Could not log HTTP request body ", e);
      }
    }

    /*
     * This will set up the logging if the response body is to be logged.
     */
    if (LOG_RESPONSE_BODY && (URI_RESPONSE_LOGGING_FILTER == null
        || URI_RESPONSE_LOGGING_FILTER.contains(httpServletRequest.getRequestURI()))) {
      try {
        final BufferedHttpServletResponse bHttpServletResponse = new BufferedHttpServletResponse(
            httpServletResponse);
        httpServletResponse = bHttpServletResponse;
      } catch (IOException e) {
        LOGGER.error("Could not log HTTP response body", e);
      }

    }

    // Log the Access Request
    HttpRequestLogging.logHttpRequest(httpServletRequest, true,
        LogResourceConfig.RequestLogConfig.getHeaderFilter(), LOG_REQUEST_BODY, STRING_BODY_FILTERS,
        true, ACCESS_LOGGER);

    chain.doFilter(httpServletRequest, httpServletResponse);

    // Log the Access Response
    HttpRequestLogging.logHttpResponse(httpServletResponse, true,
        LogResourceConfig.ResponseLogConfig.getHeaderFilter(), LOG_RESPONSE_BODY,
        STRING_BODY_FILTERS, true, ACCESS_LOGGER);

  }

  /**
   * This initializes the Masking set that we want to mask.
   */
  @Override()
  public void init(final FilterConfig arg0) throws ServletException {
    if (EXP_TO_MASK != null) {
      for (String string : EXP_TO_MASK) {
        STRING_BODY_FILTERS.add(new RegularExpressionStringFilter(string, MASK_REPLACEMENT_STRING));
      }
    }

  }

}
