package com.sample.soa.config;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Raveesh
 * @version 1
 *
 */
public class DatabaseResourceConfig {
  /**
   * Name of the database.
   */
  @JsonProperty()
  private String name;

  /**
   * Host where database is running.
   */
  @JsonProperty()
  private List<String> hosts;

  /**
   * Bucket Name or Schema name.
   */
  @JsonProperty()
  private String datastore;

  /**
   * User name used to connect to database.
   */
  @JsonProperty()
  private String username;

  /**
   * Password used to connect.
   */
  @JsonProperty()
  private String password;

  /**
   * Port number used to connect.
   */
  @JsonProperty()
  private int port;

  /**
   *
   * @return The name of the database.
   */
  public String getName() {
    return name;
  }

  /**
   *
   * @param name Name of the database.
   */
  public void setName(final String name) {
    this.name = name;
  }

  /**
   *
   * @return Hosts where database is located.
   */
  public List<String> getHosts() {
    return hosts;
  }

  /**
   *
   * @param hosts Host where database is located.
   */
  public void setHosts(final List<String> hosts) {
    this.hosts = hosts;
  }

  /**
   *
   * @return Bucket or datastore name.
   */
  public String getDatastore() {
    return datastore;
  }

  /**
   *
   * @param datastore Bucket or datastore name to be set.
   */
  public void setDatastore(final String datastore) {
    this.datastore = datastore;
  }

  /**
   *
   * @return User name used to connect to database.
   */
  public String getUsername() {
    return username;
  }

  /**
   *
   * @param username User name used to connect to database.
   */
  public void setUsername(final String username) {
    this.username = username;
  }

  /**
   *
   * @return password.
   */
  public String getPassword() {
    return password;
  }

  /**
   *
   * @param password Password to connect to database.
   */
  public void setPassword(final String password) {
    this.password = password;
  }

  /**
   *
   * @return Port Number.
   */
  public int getPort() {
    return port;
  }

  /**
   *
   * @param port Port number used.
   */
  public void setPort(final int port) {
    this.port = port;
  }

}
