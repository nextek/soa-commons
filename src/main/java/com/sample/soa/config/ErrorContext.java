package com.sample.soa.config;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author chuck
 * @version 1
 *
 */
public final class ErrorContext {

  @NotEmpty()
  private static String context;

  /**
   * Default constructor.
   */
  private ErrorContext() {
  }

  /**
   * @param errorContext
   *          String
   */
  public static void setContext(final String errorContext) {
    if (ErrorContext.context == null) {
      ErrorContext.context = errorContext;
    }
  }

  /**
   * @return the context
   */
  public static String getContext() {
    return context;
  }
}
