package com.sample.soa.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Base Spring config class which scans the default package.
 * @author Raveesh_Rana
 * @version 1.0
 */
@Configuration()
@EnableAutoConfiguration()
@ComponentScan(basePackages = { "com.sample.soa" })
public class SpringConfig {
}
