package com.sample.soa.config;

import org.glassfish.jersey.server.ServerProperties;

import com.sample.soa.errors.mappers.AppExceptionMapper;
import com.sample.soa.errors.mappers.ConstraintViolationExceptionMapper;
import com.sample.soa.errors.mappers.DefaultExceptionMapper;
import com.sample.soa.errors.mappers.JsonMappingExceptionMapper;
import com.sample.soa.errors.mappers.JsonParseExceptionMapper;
import com.sample.soa.errors.mappers.PathParamExceptionMapper;
import com.sample.soa.errors.mappers.QueryParamExceptionMapper;
import com.sample.soa.errors.mappers.WebAppExceptionMapper;

import io.dropwizard.jersey.setup.JerseyEnvironment;

/**
 * Common error registration.
 * @author chuck
 * @version 1
 *
 */
public final class ErrorResourceConfig {

  private ErrorResourceConfig() {
  }

  /**
   * Register the mappers.
   * @param environment JerseyEnvironment
   */
  public static void registerExceptions(final JerseyEnvironment environment) {
    environment.register(AppExceptionMapper.class);
    environment.register(ConstraintViolationExceptionMapper.class);
    environment.register(QueryParamExceptionMapper.class);
    environment.register(PathParamExceptionMapper.class);
    environment.register(JsonMappingExceptionMapper.class);
    environment.register(JsonParseExceptionMapper.class);
    environment.register(WebAppExceptionMapper.class);
    environment.register(DefaultExceptionMapper.class);
    environment.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, false);
  }
}
