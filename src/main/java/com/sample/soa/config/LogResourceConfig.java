package com.sample.soa.config;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Preston
 * @version 1
 *
 */
public class LogResourceConfig {

  /**
   * Stores the configuration for Request Logging.
   */
  @JsonProperty()
  private static RequestLogConfig httpRequest;

  /**
   * Stores the configuration for Response Logging.
   */
  @JsonProperty()
  private static ResponseLogConfig httpResponse;

  /**
   * Stores the patterns which should be masked during body logging.
   */
  @JsonProperty()
  private static Set<String> maskPattern;

  /**
   *
   * @return httpRequest Returns the http Request Configuration.
   */
  public static RequestLogConfig getHttpRequest() {
    return httpRequest;
  }

  /**
   *
   * @return httpResponse Returns the http Response Configuration.
   */
  public static ResponseLogConfig getHttpResponse() {
    return httpResponse;
  }

  /**
   *
   * @param httpRequest
   *          Sets the http Request Configuration.
   */
  public void setHttpRequest(final RequestLogConfig httpRequest) {
    LogResourceConfig.httpRequest = httpRequest;
  }

  /**
   *
   * @param httpResponse
   *          Sets the http Response Configuration.
   */
  public void setHttpResponse(final ResponseLogConfig httpResponse) {
    LogResourceConfig.httpResponse = httpResponse;
  }

  /**
   * @author Preston
   * @version 1
   *
   */
  public static class RequestLogConfig extends LogConfig {

    /**
     * indicates if we want to log the body. Not logged by default.
     */
    @JsonProperty()
    private static boolean body = true;

    /**
     *
     * @return body Tells if Request should be logged.
     */
    public static boolean isBody() {
      return body;
    }

    /**
     *
     * @param body
     *          Tells if request body Should be logged.
     */
    public void setBody(final boolean body) {
      RequestLogConfig.body = body;
    }

  }

  /**
   *
   * @author Raveesh_Rana
   * @version 1.0 This is the ResponseLogConfig class.
   */
  public static class ResponseLogConfig extends LogConfig {
    /**
     * indicates if we want to log the body. Not logged by default.
     */
    @JsonProperty()
    private static boolean body = false;

    /**
     *
     * @return body Tells if Request should be logged.
     */
    public static boolean isBody() {
      return body;
    }

    /**
     *
     * @param body
     *          Tells if request body Should be logged.
     */
    public void setBody(final boolean body) {
      ResponseLogConfig.body = body;
    }

  }

  /**
   *
   * @author Raveesh_Rana
   * @version 1.0 This is the base LogConfig Class.
   */
  public static class LogConfig {

    /**
     * List of body filters that should be applied to mask the strings present in bodies while
     * logging.
     */
    @JsonProperty()
    private static Set<String> uriFilter;

    /**
     * List of body filters that should be applied to mask the strings present in bodies while
     * logging.
     */
    @JsonProperty()
    private static Set<String> headerFilter;

    /**
     *
     * @return uriFilter Strings that should be masked from request and response bodies.
     */
    public static Set<String> getUriFilter() {
      return uriFilter;
    }

    /**
     *
     * @param uriFilter
     *          Strings that should be logging request or response bodies.if this is not mentioned
     *          everything will be logged.
     */
    public void setUriFilter(final Set<String> uriFilter) {
      LogConfig.uriFilter = uriFilter;
    }

    /**
     *
     * @param headerFilter
     *          Headers that should be logged. if this is not mentioned everything will be logged.
     */
    public void setHeaderFilter(final Set<String> headerFilter) {
      LogConfig.headerFilter = headerFilter;
    }

    /**
     *
     * @return headerFilter Strings that logged from the headers.
     */
    public static Set<String> getHeaderFilter() {
      return headerFilter;
    }
  }

  /**
   *
   * @return maskPattern Strings that should be masked from request and response bodies.
   */
  public static Set<String> getMaskPattern() {
    return maskPattern;
  }

  /**
   *
   * @param maskPattern
   *          Strings that should be masked from request and response bodies.
   */
  public void setMaskPattern(final Set<String> maskPattern) {
    LogResourceConfig.maskPattern = maskPattern;
  }
}
