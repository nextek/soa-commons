package com.sample.soa.config;

import java.util.EnumSet;
import java.util.concurrent.TimeUnit;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;

import org.slf4j.LoggerFactory;

import com.codahale.metrics.JmxReporter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import com.sample.soa.filter.LogFilter;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import io.dropwizard.setup.Environment;

/**
 *
 * @author Srinivas Posinasetty
 * @version 1.0
 */
public final class SOAApplicationHelper {


  private SOAApplicationHelper() {

  }

  /**
   * Dropwizard takes its on logging. We want to override this and add our custom behavior and
   * layouts. We reset the context and add the read logback.xml file in resource directory. if
   * logback.xml is not added by extending project logback-base.xml is used to load the default
   * behavior.
   *
   * @throws Exception Exception
   */
  public static void initializeLogging() throws Exception {

    final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
    try {
      final JoranConfigurator configurator = new JoranConfigurator();
      configurator.setContext(context);
      context.reset();
      final ClassLoader classloader = SOAApplication.class.getClassLoader();
      configurator.doConfigure(classloader.getResourceAsStream("logback-base.xml"));
      configurator.doConfigure(classloader.getResourceAsStream("logback.xml"));
    } catch (JoranException ex) {
      throw new Exception("Could not load logback.xml file" + ex);
    }
  }

  /**
   * This method initializes the metrics.
   *
   * @param environment Environment
   */
  public static void initializeMetrics(final Environment environment) {

    if (SOAConfig.isReportMetricsToConsole()) {
      final Slf4jReporter reporter = Slf4jReporter.forRegistry(environment.metrics())
          .outputTo(LoggerFactory.getLogger("com.sample.metrics")).convertRatesTo(TimeUnit.SECONDS)
          .convertDurationsTo(TimeUnit.MILLISECONDS).build();
      reporter.start(1, TimeUnit.MINUTES);
    } else {
      // Run the JmxReporter
      final JmxReporter reporter = JmxReporter.forRegistry(new MetricRegistry()).build();
      reporter.start();
    }
  }

  /**
   * Register the Filter for logging. These takes care of logging incoming and outgoing requests.
   * This also sets the MDC values to be logged by logger.
   * @param environment Environment
   */
  public static void addLoggingFilter(final Environment environment) {

    final FilterRegistration.Dynamic filter = environment.servlets().addFilter("LogFilter",
        new LogFilter());
    filter.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false,
        environment.getApplicationContext().getContextPath() + "*");
  }
}
