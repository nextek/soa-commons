package com.sample.soa.config;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;

/**
 * SOAConfig.java is a POJO responsible for setting global Dropwizard settings as received from the
 * YAML configuration file and is extended by the target application.
 *
 * @author Raveesh_Rana
 * @version 1.0
 */
public class SOAConfig extends Configuration {

  /**
   * Stores the access logging configuration.
   */
  @JsonProperty()
  private static LogResourceConfig accessLogging;

  /**
   * Stores the access database configuration.
   */
  @JsonProperty()
  private static DatabaseResourceConfig primaryDatabase;

  @JsonProperty()
  private static Boolean reportMetricsToConsole = false;

  /**
   *
   * @return Boolean
   */
  public static Boolean isReportMetricsToConsole() {
    return reportMetricsToConsole;
  }

  /**
   *
   * @param reportMetricsToConsole Boolean
   */
  public void setReportMetricsToConsole(final Boolean reportMetricsToConsole) {
    SOAConfig.reportMetricsToConsole = reportMetricsToConsole;
  }

  /**
   * Returns the context name of the application.
   *
   * @return Context name of the application.
   */
  @JsonProperty()
  public String getContext() {
    return ErrorContext.getContext();
  }

  /**
   * Sets the context name of the application.
   *
   * @param context
   *          The context name of the application.
   */
//  @JsonProperty()
//  public void setContext(final String context) {
//    ErrorContext.setContext(context);
//  }

  /**
   *
   * @return accessLogging
   *            Returns access logging configuration.
   */
  @JsonProperty()
  public static LogResourceConfig getAccessLogging() {
    return accessLogging;
  }

  /**
   *
   * @param accessLogging
   *           Sets the access logging Configuration.
   */
  @JsonProperty()
  public void setAccessLogging(final LogResourceConfig accessLogging) {
    SOAConfig.accessLogging = accessLogging;
  }

  /**
   *
   * @return primaryDatabase Database Resource.
   */
  public static DatabaseResourceConfig getPrimaryDatabaseResource() {
    return primaryDatabase;
  }

  /**
   *
   * @param primaryDatabase
   *          Database Resource.
   */
  public void setPrimaryDatabaseResource(final DatabaseResourceConfig primaryDatabase) {
    SOAConfig.primaryDatabase = primaryDatabase;
  }
}
