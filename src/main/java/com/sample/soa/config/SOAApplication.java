/**
 * @author Raveesh_Rana
 * @version 1.0
 */
package com.sample.soa.config;

import java.util.Map;

import javax.ws.rs.Path;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.sample.soa.hystrix.SOAHystrixInitializer;
import com.sample.soa.hystrix.SOAHystrixResourceMethodInstrumenter;
import com.yammer.tenacity.core.bundle.TenacityBundleConfigurationFactory;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.ResourceConfigurationSourceProvider;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * SOAApplication.java is to be extended by a Dropwizard application as the entry point class. This
 * class configures global settings such as the Spring container, registers exception mapper classes
 * and logging facilities.
 *
 * @author Raveesh_Rana
 * @version 1.0
 * @param <T>
 *          The Dropwizard configuration POJO that extends SOAConfig.
 */
public abstract class SOAApplication<T extends SOAConfig> extends Application<T> {

  /**
   *
   * @return TenacityBundleConfigurationFactory
   */
  public TenacityBundleConfigurationFactory getApplicationTenacityBundle() {
    return null;
  }

  /**
   *
   * @param bootstrap bootstrap
   */
  @Override()
  public void initialize(final Bootstrap<T> bootstrap) {

    bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(
        new ResourceConfigurationSourceProvider(), new EnvironmentVariableSubstitutor()));

    SOAHystrixInitializer.initialize(bootstrap, getApplicationTenacityBundle());
  }

  /**
   * Run method is called by dropwizard. All the basic configurations are done here
   *
   * @param configuration Configuration
   * @param environment Environment
   *
   */
  @Override()
  public void run(final T configuration, final Environment environment) throws Exception {

    SOAApplicationHelper.initializeLogging();
    SOAApplicationHelper.initializeMetrics(environment);
    SOAApplicationHelper.addLoggingFilter(environment);

    // Run Spring Configuration, if not needed this function should be
    // overridden by extending
    // applications.
    this.runApp(configuration, environment);

    environment.jersey().register(new SOAHystrixResourceMethodInstrumenter());

    // Register the common exceptions mappers with Jersey
    ErrorResourceConfig.registerExceptions(environment.jersey());
  }

  /**
   * This function by default starts the spring context. Scanning the path and adding all the
   * resources. if not needed this function can be overridden in future by extending projects.
   *
   * @param soaConfig
   *          The configuration settings as per the class that extends SOAConfig.
   * @param environment
   *          The environment settings.
   * @throws Exception
   *           exception
   */
  public void runApp(final T soaConfig, final Environment environment) throws Exception {
    final AnnotationConfigApplicationContext parent = new AnnotationConfigApplicationContext();
    final AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();

    parent.refresh();
    // parent.getBeanFactory().registerSingleton("configuration",configuration);
    parent.registerShutdownHook();
    parent.start();

    // the real main app context has a link to the parent context
    ctx.setParent(parent);
    ctx.register(SpringConfig.class);
    ctx.refresh();
    ctx.registerShutdownHook();
    ctx.start();

    // Register resources to Jersey
    final Map<String, Object> resources = ctx.getBeansWithAnnotation(Path.class);
    for (Map.Entry<String, Object> entry : resources.entrySet()) {
      environment.jersey().register(entry.getValue());
    }
  }
}
