/**
 * Provides global settings and configuration common to all SOA Dropwizard applications.
 *
 * SOAApplication.java is to be extended by a Dropwizard application as the entry point class.
 * This class configures global settings such as the Spring container, registers exception mapper
 * classes and logging facilities.
 *
 * SOAConfig.java is a POJO responsible for setting global Dropwizard settings as received from
 * the YAML configuration file and is extended by the target application.
 *
 * SpringConfig.java configures Spring beans and components.
 */
package com.sample.soa.config;
