package com.sample.servlet.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

/**
 * HttpServletRequestWrapper class designed to facilitate capturing the request body while allowing
 * downstream filterChains to continue to process the request body normally.
 *
 * @author Preston
 * @version 1
 *
 */
public class BufferedHttpServletRequest extends HttpServletRequestWrapper {

  private byte[] requestBody;
  private ServletInputStream inputStream = null;
  private BufferedReader reader = null;
  private boolean getStreamCalled = false;
  private boolean getReaderCalled = false;

  /**
   * Constructor to create a new instance of the BufferedHttpServletRequest.
   *
   * @param request
   *          The original HttpServletRequest
   * @throws IOException
   *           Thrown if a problem occurs ready the stream.
   */
  public BufferedHttpServletRequest(final HttpServletRequest request) throws IOException {
    super(request);
    requestBody = IOUtils.toByteArray(request.getInputStream());
  }

  /**
   * Get the request body pulled from the original HttpServletRequest.
   *
   * @return returns a byte array containing the request body extracted from the request.
   */
  public byte[] getRequestBody() {
    return requestBody;
  }

  /**
   * Permits the caller to reset the value of the request body after some modifications have been
   * made.
   *
   * @param requestBody
   *          a byte array containing the new request body.
   */
  public void setRequestBody(final byte[] requestBody) {
    this.requestBody = requestBody;
  }

  @Override()
  public ServletInputStream getInputStream() throws IOException {
    if (getReaderCalled) {
      throw new IllegalStateException("getInputStream() called after getReader().");
    } else {
      getStreamCalled = true;
    }
    if (inputStream == null) {
      inputStream = new ResettableServletInputStream(new ByteArrayInputStream(requestBody));
    }
    return inputStream;
  }

  @Override()
  public BufferedReader getReader() throws IOException {
    if (getStreamCalled) {
      throw new IllegalStateException("getReader() called after getInputStream().");
    } else {
      getStreamCalled = true;
    }
    if (reader == null) {
      reader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(requestBody)));
    }
    return reader;
  }

  /**
   * ServletInputStream wrapper.
   *
   * @author Preston
   * @version 1
   *
   */
  private class ResettableServletInputStream extends ServletInputStream {
    private ByteArrayInputStream inputStream;

    ResettableServletInputStream(final ByteArrayInputStream inputStream) {
      this.inputStream = inputStream;
    }

    @Override()
    public boolean isFinished() {
      return inputStream.available() == 0;
    }

    @Override()
    public boolean isReady() {
      return true;
    }

    @Override()
    public void setReadListener(final ReadListener readListener) {
      throw new RuntimeException("Not implemented");
    }

    @Override()
    public int read() throws IOException {
      return inputStream.read();
    }
  }
}
