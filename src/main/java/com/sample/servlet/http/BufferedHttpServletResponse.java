package com.sample.servlet.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * HttpServletResponseWrapper class designed to facilitate capturing the response body while
 * allowing upstream filterChains to continue to process the request body normally.
 *
 * @author Preston
 * @version 1
 *
 */
public class BufferedHttpServletResponse extends HttpServletResponseWrapper {

  private final BufferedServletOutputStream outputStream;

  /**
   * Constructor to create a wrapped HttpServletResponse object.
   *
   * @param response
   *          The original HttpServletResponse.
   * @throws IOException
   *           Thrown if there is a problem reading the response stream.
   */
  public BufferedHttpServletResponse(final HttpServletResponse response) throws IOException {
    super(response);
    outputStream = new BufferedServletOutputStream(response.getOutputStream());
  }

  @Override()
  public ServletOutputStream getOutputStream() throws IOException {
    return outputStream;
  }

  @Override()
  public PrintWriter getWriter() throws IOException {
    return new PrintWriter(outputStream);
  }

  /**
   * Get the captured response body.
   *
   * @return A byte array containing the captured response body.
   */
  public byte[] getResponseBody() {
    return outputStream.toByteArray();
  }

  /**
   * Class to wrap the ServletOutputStream for capturing.
   *
   * @author Preston
   * @version 1
   *
   */
  private class BufferedServletOutputStream extends ServletOutputStream {
    private final ServletOutputStream outputStream;
    private final ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    BufferedServletOutputStream(final ServletOutputStream outputStream) {
      this.outputStream = outputStream;
    }

    @Override()
    public boolean isReady() {
      return outputStream.isReady();
    }

    @Override()
    public void setWriteListener(final WriteListener writeListener) {
      outputStream.setWriteListener(writeListener);
    }

    @Override()
    public void write(final int b) throws IOException {
      buffer.write(b);
      outputStream.write(b);
    }

    /**
     *
     * @return
     */
    public byte[] toByteArray() {
      return buffer.toByteArray();
    }
  }
}
