package com.sample.logging.http;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import net.logstash.logback.marker.Markers;

import com.sample.servlet.http.BufferedHttpServletRequest;
import com.sample.servlet.http.BufferedHttpServletResponse;
import com.sample.util.StringFilter;

/**
 * @author Preston
 * @version 1
 *
 *          Class to handle common log environment processing for HTTP requests.
 */
public final class HttpRequestLogging {
  // HTTP Request logging context attributes
  /**
   * The attribute in the log that contains the message id.
   */
  public static final String MESSAGE_ID_ATTRIBUTE = "message_id";
  /**
   * The attribute in the HTTP header that containts the message id from the upstream system.
   */
  public static final String MESSAGE_ID_HEADER = "hltMessageId";

  /**
   * The attribute in the log that contains the HTTP headers.
   */
  public static final String HTTP_HEADERS_ATTRIBUTE = "http_headers";
  // HTTP Request Entry logging context attributes
  /**
   * The attribute in the log that contains the HTTP URL.
   */
  public static final String URL_ATTRIBUTE = "url";
  /**
   * The attribute in the log that contains the HTTP verb/method.
   */
  public static final String HTTP_VERB_ATTRIBUTE = "http_verb";
  /**
   * The attribute in the log that contains the HTTP verb/method.
   */
  public static final String HTTP_BODY_ATTRIBUTE = "http_body";
  /**
   * The attribute in the log that contains the peer client ip.
   */
  public static final String PEER_IP_ATTRIBUTE = "peer_ip";
  /**
   * The header in the request that contains the real client IP.
   */
  public static final String REAL_CLIENT_IP_HEADER = "True-Client-IP";
  /**
   * The attribute in the log that contains the real client IP.
   */
  public static final String REAL_CLIENT_IP_ATTRIBUTE = "real_client_ip";
  /**
   * The attribute in the log that contains the client session id.
   */
  public static final String CLIENT_SESSION_ID_ATTRIBUTE = "client_session_id";
  /**
   * The cookie in the request that contains the client id.
   */
  public static final String CLIENT_ID_COOKIE = "hltClientId";
  /**
   * The attribute in the log that contains the client id.
   */
  public static final String CLIENT_ID_ATTRIBUTE = "client_id";
  /**
   * The attribute in the log that contains the login id.
   */
  public static final String LOGIN_ID_ATTRIBUTE = "login_id";
  /**
   * The attribute in the log that contains the user agent string.
   */
  public static final String USERAGENT_ATTRIBUTE = "useragent";
  /**
   * The attribute in the log that contains the requests URI path.
   */
  public static final String PATH_ATTRIBUTE = "path";

  // HTTP Response Exit Logging context attributes
  /**
   * The attribute in the log that contains the response time between request / response in ms.
   */
  public static final String RESPONSE_TIME_ATTRIBUTE = "response_time";
  /**
   * The attribute in the log that contains the HTTP response code.
   */
  public static final String HTTP_STATUS_CODE_ATTRIBUTE = "http_status_code";

  /**
   * The list of attributes set in the MDC for the request.
   */
  public static final String[] httpRequestMdcAttributes = { MESSAGE_ID_ATTRIBUTE };

  private static final ThreadLocal<Long> requestStartTimestamp = new ThreadLocal<Long>();
  private static final Logger LOGGER = LoggerFactory.getLogger(HttpRequestLogging.class);

  /**
   * Request logging.
   */
  private HttpRequestLogging() {
  }

  /**
   * Call this method to perform log attribute handling for httpServletRequests's. Does NOT log
   * headers or body.
   *
   * @param httpServletRequest
   *          the servlet request
   */
  public static void logHttpRequest(final HttpServletRequest httpServletRequest) {
    logHttpRequest(httpServletRequest, false, null, false, null, true, null);
  }

  /**
   * Call this method to perform log attribute handling for httpServletRequests's.
   *
   * @param httpServletRequest
   *          the servlet request
   * @param dumpRequestHeaders
   *          Whether or not to dump the request headers.
   * @param dumpRequestBody
   *          Whether or not to dump the request body.
   * @param requestHeadersToDump
   *          Which headers to dump, null for all.
   * @param logRequest
   *          Whether to log the request.
   * @param customLogger
   *          The SLF4J logger instance to use to log the request, NULL to use this classes logger.
   * @param bodyFilters
   *          List to be filtered from body.
   */
  public static void logHttpRequest(final HttpServletRequest httpServletRequest,
      final boolean dumpRequestHeaders, final Collection<String> requestHeadersToDump,
      final boolean dumpRequestBody, final Collection<StringFilter> bodyFilters,
      final boolean logRequest, final Logger customLogger) {
    logHttpRequest(httpServletRequest, dumpRequestHeaders, requestHeadersToDump, dumpRequestBody,
        bodyFilters, logRequest, customLogger, null, null, null, null);
  }

  /**
   * Call this method to perform log attribute handling for httpServletRequests's.
   *
   * @param httpServletRequest
   *          the servlet request
   * @param dumpRequestHeaders
   *          Whether or not to dump the request headers.
   * @param dumpRequestBody
   *          Whether or not to dump the request body.
   * @param requestHeadersToDump
   *          Which headers to dump, null for all.
   * @param logRequest
   *          Whether to log the request.
   * @param customLogger
   *          The SLF4J logger instance to use to log the request, NULL to use this classes logger.
   * @param bodyFilters
   *          Any string pattern that has to be filtered from the body.
   * @param realClientIpHeader
   *          Client IP header.
   * @param clientIdHeader
   *          Client ID Header.
   * @param clientSessionId
   *          Client Session Id Header.
   * @param loginId
   *          Login Id.
   */
  public static void logHttpRequest(final HttpServletRequest httpServletRequest,
      final boolean dumpRequestHeaders, final Collection<String> requestHeadersToDump,
      final boolean dumpRequestBody, final Collection<StringFilter> bodyFilters,
      final boolean logRequest, final Logger customLogger, final String realClientIpHeader,
      final String clientIdHeader, final String clientSessionId, final String loginId) {
    // MDC attributes that get set for downstream logging
    setMDCForHttpRequest(httpServletRequest);

    final Logger tempLogger;
    if (logRequest) {
      if (customLogger != null) {
        tempLogger = customLogger;
      } else {
        tempLogger = LOGGER;
      }
      tempLogger.info(Markers.appendEntries(getHttpRequestLogOnlyMap(httpServletRequest,
          dumpRequestHeaders, requestHeadersToDump, dumpRequestBody, bodyFilters,
          realClientIpHeader, clientIdHeader, clientSessionId, loginId)), "HTTP Request Log Entry");
    }
    // Save the time stamp when this request was processed, will be used later for response
    // processing.
    requestStartTimestamp.set(System.currentTimeMillis());
  }

  /**
   * Call this method to perform log attribute handling for httpServletResponse's. Defaults to NOT
   * logging headers or body.
   *
   * @param httpServletResponse
   *          the servlet response.
   */
  public static void logHttpResponse(final HttpServletResponse httpServletResponse) {
    logHttpResponse(httpServletResponse, false, null, false, null, true, null);
  }

  /**
   * Call this method to perform log attribute handling for httpServletResponse's.
   *
   * @param httpServletResponse
   *          the servlet response.
   * @param dumpResponseHeaders
   *          Whether or not to dump the response headers.
   * @param dumpResponseBody
   *          Whether or not to dump the response body.
   * @param responseHeadersToDump
   *          Which headers to dump, null for all.
   * @param logResponse
   *          Whether to log the response.
   * @param customLogger
   *          The SLF4J logger instance to use to log the response, NULL to use this classes logger.
   * @param bodyFilters
   *          Collection to be filtered from body.
   */
  public static void logHttpResponse(final HttpServletResponse httpServletResponse,
      final boolean dumpResponseHeaders, final Collection<String> responseHeadersToDump,
      final boolean dumpResponseBody, final Collection<StringFilter> bodyFilters,
      final boolean logResponse, final Logger customLogger) {
    final Logger tempLogger;
    if (logResponse) {
      if (customLogger != null) {
        tempLogger = customLogger;
      } else {
        tempLogger = LOGGER;
      }
      tempLogger.info(Markers.appendEntries(getHttpResponseLogOnlyMap(httpServletResponse,
          dumpResponseHeaders, responseHeadersToDump, dumpResponseBody, bodyFilters)),
          "HTTP Response Log Entry");
    }
    // Clear our the remaining MDC attributes set by this function
    clearMDCValuesFor(httpRequestMdcAttributes);
  }

  private static void setMDCForHttpRequest(final HttpServletRequest httpServletRequest) {
    String messagId = httpServletRequest.getHeader(MESSAGE_ID_HEADER);
    if (messagId == null) {
      messagId = generateMessageId();
    }
    MDC.put(MESSAGE_ID_ATTRIBUTE, messagId);
  }

  /**
   * This method should be called by applications to retrieve (or generate) the message ID to place
   * into HTTP headers for message correlation across HTTP calls.
   *
   * @return String Distinct message Id for any incoming server request.
   */
  public static String getMessageId() {
    final String messageId = MDC.get(MESSAGE_ID_ATTRIBUTE);
    if (messageId != null) {
      return messageId;
    }
    return generateMessageId();
  }

  private static String generateMessageId() {
    return UUID.randomUUID().toString();
  }

  private static Map<String, Object> getHttpRequestLogOnlyMap(
      final HttpServletRequest httpServletRequest, final boolean dumpRequestHeaders,
      final Collection<String> requestHeadersToDump, final boolean dumpRequestBody,
      final Collection<StringFilter> bodyFilters, final String realClientIpHeader,
      final String clientIdCookie, final String clientSessionId, final String loginId) {
    final Map<String, Object> m = new HashMap<>();

    m.put(PATH_ATTRIBUTE, httpServletRequest.getRequestURI());
    m.put(HTTP_VERB_ATTRIBUTE, httpServletRequest.getMethod());
    m.put(PEER_IP_ATTRIBUTE, httpServletRequest.getRemoteAddr());
    m.put(USERAGENT_ATTRIBUTE, httpServletRequest.getHeader("User-Agent"));

    final String realClientIp = getRealClientIp(httpServletRequest, realClientIpHeader);
    if (realClientIp != null) {
      m.put(REAL_CLIENT_IP_ATTRIBUTE, realClientIp);
    }

    final String clientId = getClientId(httpServletRequest, clientIdCookie);
    if (clientId != null) {
      m.put(CLIENT_ID_ATTRIBUTE, clientId);
    }

    if (clientSessionId != null) {
      m.put(CLIENT_SESSION_ID_ATTRIBUTE, clientSessionId);
    }
    if (loginId != null) {
      m.put(LOGIN_ID_ATTRIBUTE, loginId);
    }

    setRequestHeaders(dumpRequestHeaders, m, httpServletRequest, requestHeadersToDump);
    setRequestBody(dumpRequestBody, m, httpServletRequest, bodyFilters);

    return m;
  }

  private static Map<String, Object> getHttpResponseLogOnlyMap(
      final HttpServletResponse httpServletResponse, final boolean dumpResponseHeaders,
      final Collection<String> responseHeadersToDump, final boolean dumpResponseBody,
      final Collection<StringFilter> bodyFilters) {
    final Map<String, Object> m = new HashMap<String, Object>();
    // set the response time for the request
    if (requestStartTimestamp.get() != null) {
      m.put(RESPONSE_TIME_ATTRIBUTE, System.currentTimeMillis() - requestStartTimestamp.get());
    } else {
      m.put(RESPONSE_TIME_ATTRIBUTE, "Unknown");
    }

    // set the response status code
    m.put(HTTP_STATUS_CODE_ATTRIBUTE, httpServletResponse.getStatus());

    // If debug is enabled, log the response headers
    if (dumpResponseHeaders) {
      final Map<String, Object> h = new HashMap<>();
      Collection<String> tempResponseHeadersToDump = responseHeadersToDump;
      if (tempResponseHeadersToDump == null) {
        tempResponseHeadersToDump = httpServletResponse.getHeaderNames();
      }
      // Put the headers in the context
      for (final String headerName : tempResponseHeadersToDump) {
        h.put(headerName, httpServletResponse.getHeader(headerName));
      }
      m.put(HTTP_HEADERS_ATTRIBUTE, h);
    }
    Collection<StringFilter> tempBodyFilters = bodyFilters;
    if (tempBodyFilters == null) {
      tempBodyFilters = Collections.emptyList();
    }
    if (dumpResponseBody && httpServletResponse instanceof BufferedHttpServletResponse) {
      String body = new String(
          ((BufferedHttpServletResponse) httpServletResponse).getResponseBody());
      for (StringFilter sf : tempBodyFilters) {
        body = sf.filterString(body);
      }
      m.put(HTTP_BODY_ATTRIBUTE, body);
    }
    return m;
  }

  private static void clearMDCValuesFor(final String[] keyValues) {
    for (String key : keyValues) {
      MDC.remove(key);
    }
  }

  private static String getFirstCookieValue(final HttpServletRequest request,
      final String cookieName) {
    if (request == null || cookieName == null || cookieName.length() == 0) {
      return (null);
    }
    Cookie[] cookies = request.getCookies();
    if (cookies == null) {
      cookies = new Cookie[0];
    }
    for (Cookie c : cookies) {
      if (cookieName.equals(c.getName())) {
        return (c.getValue());
      }
    }
    return (null);
  }

  private static String getRealClientIp(final HttpServletRequest httpServletRequest,
      final String clientIpHeader) {
    String realClientIp = null;

    if (clientIpHeader != null) {
      realClientIp = httpServletRequest.getHeader(clientIpHeader);
    } else {
      realClientIp = httpServletRequest.getHeader(REAL_CLIENT_IP_HEADER);
    }

    return realClientIp;
  }

  private static String getClientId(final HttpServletRequest httpServletRequest,
      final String clientIdCookie) {
    String clientId = null;

    if (clientIdCookie != null) {
      clientId = getFirstCookieValue(httpServletRequest, clientIdCookie);
    } else {
      clientId = getFirstCookieValue(httpServletRequest, CLIENT_ID_COOKIE);
    }

    return clientId;
  }

  private static void setRequestHeaders(final boolean dumpRequestHeaders,
      final Map<String, Object> mapToSet, final HttpServletRequest httpServletRequest,
      final Collection<String> requestHeadersToDump) {
    final Map<String, Object> h = new HashMap<>();
    final List<String> tempRequestHeadersToDump = new ArrayList<String>();
    if (requestHeadersToDump != null) {
      tempRequestHeadersToDump.addAll(requestHeadersToDump);
    } else {
      for (Enumeration<String> e = httpServletRequest.getHeaderNames(); e.hasMoreElements();) {
        final String headerName = e.nextElement();
        tempRequestHeadersToDump.add(headerName);
      }
    }

    if (dumpRequestHeaders) {
      // Put the headers in the context
      for (final String headerName : tempRequestHeadersToDump) {
        h.put(headerName, httpServletRequest.getHeader(headerName));
      }
    }
    mapToSet.put(HTTP_HEADERS_ATTRIBUTE, h);
  }

  private static void setRequestBody(final boolean dumpRequestBody,
      final Map<String, Object> mapToSet, final HttpServletRequest httpServletRequest,
      final Collection<StringFilter> bodyFilters) {
    Collection<StringFilter> tempBodyFilters = bodyFilters;
    if (tempBodyFilters == null) {
      tempBodyFilters = Collections.emptyList();
    }
    if (dumpRequestBody && httpServletRequest instanceof BufferedHttpServletRequest) {
      String bodyAttributes = new String(
          ((BufferedHttpServletRequest) httpServletRequest).getRequestBody());
      for (StringFilter sf : tempBodyFilters) {
        bodyAttributes = sf.filterString(bodyAttributes);
      }
      mapToSet.put(HTTP_BODY_ATTRIBUTE, bodyAttributes);
    }
  }
}
