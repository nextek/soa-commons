package com.sample.util;

/**
 * Class to handle simple regular expression string replacement using the StringFilter interface so
 * that it can be passed in as a list chain to various processing systems.
 *
 * @author Preston
 * @version 1
 *
 */
public class RegularExpressionStringFilter implements StringFilter {
  private final String regularExpression;
  private final String replacementText;

  /**
   * Constructor to create a string filter for chain processing.
   *
   * @param expression
   *          The search expression
   * @param replacementText
   *          the replace expression
   */
  public RegularExpressionStringFilter(final String expression, final String replacementText) {
    regularExpression = expression;
    this.replacementText = replacementText;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.sample.util.StringFilter#filterString(java.lang.String)
   */
  @Override()
  public String filterString(final String stringToFilter) {
    if (stringToFilter == null) {
      return null;
    }
    return stringToFilter.replaceAll(regularExpression, replacementText);
  }
}
