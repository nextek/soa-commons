/**
 *
 */
package com.sample.util.cucumber;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public final class StepUtils {

  /**
   *
   */
  public static final String NULL = "null";
  /**
   *
   */
  public static final String EMPTY = "empty";
  /**
   *
   */
  public static final String EMPTY_STRING = "";

  private StepUtils() {
    // safety constructor
  }

  /**
   *
   * @param value string
   * @return String
   */
  public static String asNullOrEmpty(final String value) {
    String retVal;

    if (value == null || NULL.equalsIgnoreCase(value)) {
      retVal = null;
    } else {
      if (EMPTY_STRING.equals(value) || EMPTY.equalsIgnoreCase(value)) {
        retVal = EMPTY_STRING;
      } else {
        retVal = value;
      }
    }

    return retVal;
  }
}
