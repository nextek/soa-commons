/**
 *
 */
package com.sample.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public interface Converters {
  /**
   *
   * @param dateTime DateTime
   * @return ZonedDateTime
   */
  static String toUTC(final String dateTime) {
    final ZonedDateTime zonedDateTime = ZonedDateTime.parse(dateTime,
        DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    return ZonedDateTime.ofInstant(zonedDateTime.toInstant(), ZoneId.of("UTC"))
        .format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
  }
}
