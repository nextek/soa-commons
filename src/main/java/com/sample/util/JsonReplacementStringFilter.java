package com.sample.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

/**
 * Class to handle string replacement in JSON documents using the StringFilter interface so that it
 * can be passed in as a list chain to various processing systems.
 *
 * @author Preston
 * @version 1
 *
 */
public class JsonReplacementStringFilter implements StringFilter {
  private static final Logger LOGGER = LoggerFactory.getLogger(JsonReplacementStringFilter.class);

  private final List<String> jsonPointers = new ArrayList<String>();
  private final List<String> searches = new ArrayList<String>();
  private final List<String> replaces = new ArrayList<String>();

  @Override()
  public String filterString(final String stringToFilter) {
    final JsonNode jsonFromString;
    LOGGER.debug("Performing filter on string=\"{}\"", stringToFilter);

    try {
      jsonFromString = new ObjectMapper().readTree(stringToFilter);
    } catch (Exception e) {
      LOGGER.error("Unable to parse string as a JSON object.  Returning string unmodified.", e);
      return stringToFilter;
    }

    executeNodeReplacements(jsonFromString);

    final ObjectMapper om = new ObjectMapper();

    om.enable(SerializationFeature.INDENT_OUTPUT);
    final ObjectWriter ow = om.writer().withDefaultPrettyPrinter();

    try {
      return ow.writeValueAsString(jsonFromString);
    } catch (JsonProcessingException e) {
      final String s = "Unable to process the modified JSON document into a string.  "
          + "Returning string unmodified.";
      LOGGER.error(s, e);
    }
    return stringToFilter;
  }

  private void executeNodeReplacements(final JsonNode document) {
    for (int i = 0; i < jsonPointers.size(); i++) {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug(
            "Processing node replacement JsonPointer=\"{}\", Search=\"{}\", Replace=\"{}\"",
            jsonPointers.get(i), searches.get(i), replaces.get(i));
      }

      final JsonPointer jp = JsonPointer.compile(jsonPointers.get(i));

      LOGGER.debug("Compile JSON Pointer={}", jp);

      final JsonNode replaceCandidate = document.at(jp);
      if (replaceCandidate != null && !replaceCandidate.isMissingNode()) {
        LOGGER.debug("Candidate replacement node={}", replaceCandidate);
        replaceNode(document, replaceCandidate, i, jp);
      }
    }
  }

  private void replaceNode(final JsonNode document, final JsonNode replaceCandidate,
      final int actionIndex, final JsonPointer jsonPointer) {
    final JsonNode parentNode = document.at(jsonPointer.head());
    LOGGER.debug("parenNode={}", parentNode);
    if (parentNode instanceof ObjectNode) {
      replaceNodeOnObject(parentNode, jsonPointer.getMatchingProperty(), actionIndex);
    } else if (parentNode instanceof ArrayNode) {
      replaceNodeOnArray(parentNode, replaceCandidate, jsonPointer.tail().getMatchingProperty(),
          actionIndex);
    }
  }

  private void replaceNodeOnObject(final JsonNode parentNode, final String nodeName,
      final int actionIndex) {
    LOGGER.debug("nodeName={}", nodeName);
    final ObjectNode parentObject = (ObjectNode) parentNode;
    final JsonNode targetNode = parentObject.get(nodeName);
    if (searches.get(actionIndex) == null || !targetNode.isTextual()) {
      LOGGER.debug("Performing simple node replacement");
      parentObject.put(nodeName, replaces.get(actionIndex));
    } else {
      final String oldValue = targetNode.asText();
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Performing replaceAll(\"{}\",\"{}\") on text value of node={}",
            searches.get(actionIndex), replaces.get(actionIndex), oldValue);
      }
      parentObject.put(nodeName,
          oldValue.replaceAll(searches.get(actionIndex), replaces.get(actionIndex)));
    }
  }

  private void replaceNodeOnArray(final JsonNode parentNode, final JsonNode replaceCandidate,
      final String nodeName, final int actionIndex) {
    LOGGER.debug("nodeName={}", nodeName);
    final ArrayNode parentArray = (ArrayNode) parentNode;
    final int pos = Integer.parseInt(nodeName);
    LOGGER.debug("Replacing node in array position={}", pos);
    if (searches.get(actionIndex) == null || !replaceCandidate.isTextual()) {
      LOGGER.debug("Performing simple node replacement");
      parentArray.set(pos, new TextNode(replaces.get(actionIndex)));
    } else {
      final String oldValue = replaceCandidate.asText();
      LOGGER.debug("Performing replaceAll(\"{}\",\"{}\") on text value of node={}",
          searches.get(actionIndex), replaces.get(actionIndex), oldValue);
      parentArray.set(pos,
          new TextNode(oldValue.replaceAll(searches.get(actionIndex), replaces.get(actionIndex))));
    }
  }

  /**
   * Method used to add a JsonPointer, search string and replacement string on the processor.
   *
   * @param jsonPointer
   *          A JSON Pointer used to find the element you wish to process.
   * @param search
   *          The search regular expression you wish to use on the value of the jsonPointer.
   * @param replace
   *          The text you wish to use replace the search result with on the value of the
   *          jsonPointer.
   */
  public void addPointerReplacement(final String jsonPointer, final String search,
      final String replace) {
    jsonPointers.add(jsonPointer);
    searches.add(search);
    replaces.add(replace);
  }
}
