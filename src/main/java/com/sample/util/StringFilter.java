package com.sample.util;

/**
 * Interface to permit arbitrary string filter classes to be passed into objects that wish to
 * perform string modification.
 *
 * @author Preston
 * @version 1
 *
 */
public interface StringFilter {
  /**
   * Consuming objects will call this method to process a string based on an array of StringFilters
   * ultimately ending up with a string modified as desired by the caller.
   *
   * @param stringToFilter
   *          The string the caller wishes to filter.
   * @return The filtered string value.
   */
  String filterString(String stringToFilter);
}
