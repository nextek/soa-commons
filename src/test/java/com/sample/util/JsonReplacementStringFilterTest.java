/**
 *
 */
package com.sample.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.util.JsonReplacementStringFilter;

/**
 * @author Preston
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class JsonReplacementStringFilterTest {

  @Test()
  public void testFilterString() throws IOException {
    JsonReplacementStringFilter jsonReplacementStringFilter = new JsonReplacementStringFilter();
    jsonReplacementStringFilter.addPointerReplacement("/test1", null, "foo");

    String testText = IOUtils.toString(this.getClass().getResourceAsStream("/com/sample/util/test.json"));

    String replacedText = jsonReplacementStringFilter.filterString(testText);

    assert( jsonNodeStringsEqual(replacedText, this.getClass().getResourceAsStream("/com/sample/util/testResult1.json")));
  }

  @Test()
  public void testArray() throws IOException {
    JsonReplacementStringFilter jsonReplacementStringFilter = new JsonReplacementStringFilter();
    jsonReplacementStringFilter.addPointerReplacement("/array/1", null, "foo");

    String testText = IOUtils.toString(this.getClass().getResourceAsStream("/com/sample/util/testarray.json"));

    String replacedText = jsonReplacementStringFilter.filterString(testText);

    assert( jsonNodeStringsEqual(replacedText, this.getClass().getResourceAsStream("/com/sample/util/testarrayResult1.json")));
  }

  private boolean jsonNodeStringsEqual( final String doc1, final InputStream doc2 ) throws JsonProcessingException, IOException{
    final ObjectMapper om = new ObjectMapper();

    JsonNode result = om.readTree(doc1);
    JsonNode answer = om.readTree(doc2);

    return answer.equals(result);
  }
}
