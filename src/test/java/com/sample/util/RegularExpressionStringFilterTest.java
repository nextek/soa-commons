/**
 * 
 */
package com.sample.util;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.sample.util.RegularExpressionStringFilter;

/**
 * @author Preston
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class RegularExpressionStringFilterTest {

  private static final String TESTSTRING = "testfoobartest";

  @Test()
  public void testFilterString() throws IOException {

    assert ("blafoobarbla".equals(
        new RegularExpressionStringFilter("test", "bla").filterString(TESTSTRING)));
    assert ("testblabartest".equals(
        new RegularExpressionStringFilter("foo", "bla").filterString(TESTSTRING)));
    assert ("testfooblatest".equals(
        new RegularExpressionStringFilter("bar", "bla").filterString(TESTSTRING)));
    assert ( new RegularExpressionStringFilter("bar", "bla").filterString(null) == null);
  }

}
