/**
 *
 */
package com.sample.util;

import org.junit.Test;

import com.sample.util.cucumber.StepUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class StepUtilsTest {

  private static final String REAL_VALUE = "blue";

  /**
   *
   */
  @Test()
  public void test() {
    assertEquals(null, StepUtils.asNullOrEmpty(StepUtils.NULL));
    assertEquals(null, StepUtils.asNullOrEmpty(null));

    assertEquals("", StepUtils.asNullOrEmpty(StepUtils.EMPTY));
    assertEquals("", StepUtils.asNullOrEmpty(""));

    assertNotEquals(null, StepUtils.asNullOrEmpty(StepUtils.EMPTY));
    assertNotEquals("", StepUtils.asNullOrEmpty(StepUtils.NULL));
    assertNotEquals(null, StepUtils.asNullOrEmpty(""));
    assertNotEquals("", StepUtils.asNullOrEmpty(null));

    assertEquals(REAL_VALUE, StepUtils.asNullOrEmpty(REAL_VALUE));
  }

}
