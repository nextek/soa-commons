package com.sample.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sample.util.Converters;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class ConvertersTest {

  @BeforeClass()
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass()
  public static void tearDownAfterClass() throws Exception {
  }

  @Before()
  public void setUp() throws Exception {
  }

  @After()
  public void tearDown() throws Exception {
  }

  @Test()
  public void testToUTC() {
    final String input = "2016-01-01T01:00:00-05:00";
    final String expected = "2016-01-01T06:00:00Z";

    final String result = Converters.toUTC(input);

    assertEquals(expected, result);
  }

}
