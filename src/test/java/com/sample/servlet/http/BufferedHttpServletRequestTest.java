/**
 * 
 */
package com.sample.servlet.http;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sample.servlet.http.BufferedHttpServletRequest;

/**
 * @author Preston
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class BufferedHttpServletRequestTest {
  private static final String testBody = "This is a test of the body";

  @Test
  public void testFilterString() throws IOException {
    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest();

    BufferedHttpServletRequest bufferedHttpServletRequest = new BufferedHttpServletRequest(mockHttpServletRequest);
    
    final byte[] getBody = bufferedHttpServletRequest.getRequestBody();
    final byte[] readBody = IOUtils.toByteArray(bufferedHttpServletRequest.getInputStream());
    assert( Arrays.equals(testBody.getBytes(), getBody));
    assert( Arrays.equals(getBody, readBody));
  }

  private HttpServletRequest getMockHttpServletRequest() throws IOException{
    HttpServletRequest mockHttpServletRequest = Mockito.mock(HttpServletRequest.class);
    Mockito.when(mockHttpServletRequest.getInputStream()).thenReturn(new ResettableServletInputStream(new ByteArrayInputStream(testBody.getBytes())));
    Mockito.when(mockHttpServletRequest.getReader()).thenReturn(
        new BufferedReader(
            new InputStreamReader(
                new ByteArrayInputStream(testBody.getBytes())
            )
        )
    );
    
    return mockHttpServletRequest;
  }
  

}
