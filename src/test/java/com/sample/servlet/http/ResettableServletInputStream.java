package com.sample.servlet.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;

public class ResettableServletInputStream extends ServletInputStream {
  private ByteArrayInputStream inputStream;

  public ResettableServletInputStream(final ByteArrayInputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public boolean isFinished() {
    return inputStream.available() == 0;
  }

  @Override
  public boolean isReady() {
    return true;
  }

  @Override
  public void setReadListener(final ReadListener readListener) {
    throw new RuntimeException("Not implemented");
  }

  @Override
  public int read() throws IOException {
    return inputStream.read();
  }
}
