/**
 * 
 */
package com.sample.servlet.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.sample.servlet.http.BufferedHttpServletResponse;

/**
 * @author Preston
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class BufferedHttpServletResponseTest {
  private static final String testBody = "This is a test of the body";
  
  private ByteArrayOutputStream out = new ByteArrayOutputStream();

  @Test
  public void testBufferedHttpServletResponse() throws IOException {
    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();

    BufferedHttpServletResponse bufferedHttpServletResponse = new BufferedHttpServletResponse(mockHttpServletResponse);
    bufferedHttpServletResponse.getOutputStream().write(testBody.getBytes());
    
    final byte[] getBody = bufferedHttpServletResponse.getResponseBody();
    
    
    assert( Arrays.equals(testBody.getBytes(), getBody));
    assert( Arrays.equals(getBody, out.toByteArray()));
  }

  private HttpServletResponse getMockHttpServletResponse() throws IOException{
    HttpServletResponse mockHttpServletResponse = Mockito.mock(HttpServletResponse.class);
    Mockito.when(mockHttpServletResponse.getOutputStream()).thenReturn(new BufferedServletOutputStream(out));
    Mockito.when(mockHttpServletResponse.getWriter()).thenReturn((new PrintWriter(new OutputStreamWriter(out))));
    
    return mockHttpServletResponse;
  }
}
