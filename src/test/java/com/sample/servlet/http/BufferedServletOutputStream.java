package com.sample.servlet.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;

public class BufferedServletOutputStream extends ServletOutputStream {
  private final OutputStream outputStream;
  private final ByteArrayOutputStream buffer = new ByteArrayOutputStream();

  BufferedServletOutputStream(final OutputStream outputStream) {
    this.outputStream = outputStream;
  }

  @Override
  public boolean isReady() {
    return true;
  }

  @Override
  public void setWriteListener(final WriteListener writeListener) {
    
  }

  @Override
  public void write(final int b) throws IOException {
    buffer.write(b);
    outputStream.write(b);
  }

  public byte[] toByteArray() {
    return buffer.toByteArray();
  }
}
