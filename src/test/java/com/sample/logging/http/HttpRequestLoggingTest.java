package com.sample.logging.http;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

import org.apache.commons.collections4.IteratorUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.Marker;

import com.sample.logging.http.HttpRequestLogging;
import com.sample.servlet.http.BufferedHttpServletRequest;
import com.sample.servlet.http.BufferedHttpServletResponse;
import com.sample.servlet.http.ResettableServletInputStream;
import com.sample.util.RegularExpressionStringFilter;
import com.sample.util.StringFilter;

import net.logstash.logback.marker.MapEntriesAppendingMarker;

/**
 * For more information about mocking classes using Mockito, refer to http://mockito.org.
 */

@RunWith(MockitoJUnitRunner.class)
public class HttpRequestLoggingTest {
  private static long sleepTime = 100L;
  private static int returnCode = 200;
  private static String TEST_CONTEXT_PATH = "/test/Context/Path";
  private static String TEST_HTTP_METHOD = "GET";
  private static String TEST_PEER_IP = "192.168.1.1";
  private static String TEST_REAL_CLIENT_IP = "10.0.1.1";
  private static String ALT_REAL_CLIENT_IP_HEADER = "REALCLIENTIP";
  private static String TEST_CLIENT_ID = "TESTCLIENTID";
  private static String ALT_CLIENT_ID_COOKIE = "CLIENTID";
  private static String TEST_CLIENT_SESSION_ID = "TESTCLIENTSESSIONID";
  private static String TEST_LOGIN_ID = "TESTLOGINID";
  private static String TEST_USER_AGENT = "IE is dead!";
  private static String TEST_BODY = "This is a test body!";
  private static String TEST_MESSAGEID = "TESTMESSAGEID";

  private static final Appender mockAppender;

  static {
    ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(
        ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
    mockAppender = Mockito.mock(Appender.class);
    Mockito.when(mockAppender.getName()).thenReturn("MOCK");
    root.addAppender(mockAppender);
  }

  @SuppressWarnings("unchecked")
  @Test
  public void handleBasicHttpRequestResponseTest() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, true);
    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();

    HttpRequestLogging.logHttpRequest(mockHttpServletRequest);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (TEST_CONTEXT_PATH.equals(m.get(HttpRequestLogging.PATH_ATTRIBUTE)) &&
                        TEST_HTTP_METHOD.equals(m.get(HttpRequestLogging.HTTP_VERB_ATTRIBUTE)) &&
                        TEST_PEER_IP.equals(m.get(HttpRequestLogging.PEER_IP_ATTRIBUTE)) &&
                        TEST_USER_AGENT.equals(m.get(HttpRequestLogging.USERAGENT_ATTRIBUTE)) &&
                        TEST_REAL_CLIENT_IP.equals(m.get(HttpRequestLogging.REAL_CLIENT_IP_ATTRIBUTE)) &&
                        TEST_CLIENT_ID.equals(m.get(HttpRequestLogging.CLIENT_ID_ATTRIBUTE)) &&
                        m.get(HttpRequestLogging.CLIENT_SESSION_ID_ATTRIBUTE) == null &&
                        m.get(HttpRequestLogging.LOGIN_ID_ATTRIBUTE) == null ) {
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));

    final String message_id = MDC.get(HttpRequestLogging.MESSAGE_ID_ATTRIBUTE);
    assert (message_id != null);
    assert (message_id.length() == 36);
    assert (message_id.matches(
        "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"));
    Thread.sleep(sleepTime);

    Mockito.reset(mockAppender);

    HttpRequestLogging.logHttpResponse(mockHttpServletResponse);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.get(HttpRequestLogging.RESPONSE_TIME_ATTRIBUTE) != null &&
                        m.get(HttpRequestLogging.RESPONSE_TIME_ATTRIBUTE) instanceof Long &&
                        (Long) m.get(HttpRequestLogging.RESPONSE_TIME_ATTRIBUTE) >= sleepTime &&
                        m.get(HttpRequestLogging.HTTP_STATUS_CODE_ATTRIBUTE) != null &&
                        m.get(HttpRequestLogging.HTTP_STATUS_CODE_ATTRIBUTE) instanceof Integer &&
                        (Integer) m.get(
                            HttpRequestLogging.HTTP_STATUS_CODE_ATTRIBUTE) == returnCode) {
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
                return false;
              } else {
                return false;
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestHeadersTestAll() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, true);

    HttpRequestLogging.logHttpRequest(mockHttpServletRequest, true, null, false, null, true, null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE)) {
                      Map<?, ?> h = (Map<?, ?>) m.get(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE);
                      for ( Enumeration<String> e = mockHttpServletRequest.getHeaderNames() ; e.hasMoreElements() ;) {
                        String key = e.nextElement();
                        if ( mockHttpServletRequest.getHeader(key) == null) {
                          if (h.get(key) != null) {
                            return false;
                          }
                        } else {
                          if (! mockHttpServletRequest.getHeader(key).equals(h.get(key))) {
                            return false;
                          }
                        }
                      }
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestHeadersTestSubset() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    List<String> only = new ArrayList<>();
    only.add("test1");

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, true);

    HttpRequestLogging.logHttpRequest(mockHttpServletRequest, true, only, false, null, true, null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE)) {
                      Map<?, ?> h = (Map<?, ?>) m.get(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE);
                      if (h.size() != 1) {
                        return false;
                      }
                      if (!"value1".equals(h.get("test1"))) {
                        return false;
                      }
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpResponseHeadersTestAll() throws InterruptedException {
    Mockito.reset(mockAppender);

    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();

    HttpRequestLogging.logHttpResponse(
        mockHttpServletResponse,
        true,
        null,
        false,
        null,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE)) {
                      Map<?, ?> h = (Map<?, ?>) m.get(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE);
                      for ( String key : mockHttpServletResponse.getHeaderNames()) {
                        if ( mockHttpServletResponse.getHeader(key) == null) {
                          if (h.get(key) != null) {
                            return false;
                          }
                        } else {
                          if (! mockHttpServletResponse.getHeader(key).equals(h.get(key))) {
                            return false;
                          }
                        }
                      }
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpResponseHeadersTestSubset() throws InterruptedException {
    Mockito.reset(mockAppender);

    List<String> only = new ArrayList<>();
    only.add("test1");

    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();

    HttpRequestLogging.logHttpResponse(
        mockHttpServletResponse,
        true,
        only,
        false,
        null,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE)) {
                      Map<?, ?> h = (Map<?, ?>) m.get(HttpRequestLogging.HTTP_HEADERS_ATTRIBUTE);
                      if (h.size() != 1) {
                        return false;
                      }
                      if (!"value1".equals(h.get("test1"))) {
                        return false;
                      }
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestBodyTest() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, true);
    BufferedHttpServletRequest mockBufferedHttpServletRequest = new BufferedHttpServletRequest(
        mockHttpServletRequest);

    HttpRequestLogging.logHttpRequest(
        mockBufferedHttpServletRequest,
        true,
        null,
        true,
        null,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_BODY_ATTRIBUTE)) {
                      String body = (String) m.get(HttpRequestLogging.HTTP_BODY_ATTRIBUTE);
                      return TEST_BODY.equals(body);
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpResponseBodyTest() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();
    BufferedHttpServletResponse mockBufferedHttpServletResponse = new BufferedHttpServletResponse(
        mockHttpServletResponse);

    mockBufferedHttpServletResponse.getOutputStream().write(TEST_BODY.getBytes());

    HttpRequestLogging.logHttpResponse(
        mockBufferedHttpServletResponse,
        true,
        null,
        true,
        null,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_BODY_ATTRIBUTE)) {
                      String body = (String) m.get(HttpRequestLogging.HTTP_BODY_ATTRIBUTE);
                      return TEST_BODY.equals(body);
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestBodyFilterTest() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, true);
    BufferedHttpServletRequest mockBufferedHttpServletRequest = new BufferedHttpServletRequest(
        mockHttpServletRequest);

    final RegularExpressionStringFilter resf = new RegularExpressionStringFilter("This", "That");
    final RegularExpressionStringFilter resf2 = new RegularExpressionStringFilter("body",
        "responsebody");
    final List<StringFilter> l = new ArrayList<>();
    l.add(resf);
    l.add(resf2);

    final String convertedValue = TEST_BODY.replaceAll("This", "That").replaceAll(
        "body",
        "responsebody");

    HttpRequestLogging.logHttpRequest(
        mockBufferedHttpServletRequest,
        true,
        null,
        true,
        l,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_BODY_ATTRIBUTE)) {
                      String body = (String) m.get(HttpRequestLogging.HTTP_BODY_ATTRIBUTE);
                      return convertedValue.equals(body);
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpResponseBodyFilterTest() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletResponse mockHttpServletResponse = getMockHttpServletResponse();
    BufferedHttpServletResponse mockBufferedHttpServletResponse = new BufferedHttpServletResponse(
        mockHttpServletResponse);

    final RegularExpressionStringFilter resf = new RegularExpressionStringFilter("This", "That");
    final RegularExpressionStringFilter resf2 = new RegularExpressionStringFilter("body",
        "responsebody");
    final List<StringFilter> l = new ArrayList<>();
    l.add(resf);
    l.add(resf2);

    final String convertedValue = TEST_BODY.replaceAll("This", "That").replaceAll(
        "body",
        "responsebody");

    mockBufferedHttpServletResponse.getOutputStream().write(TEST_BODY.getBytes());

    HttpRequestLogging.logHttpResponse(
        mockBufferedHttpServletResponse,
        true,
        null,
        true,
        l,
        true,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if (m.containsKey(HttpRequestLogging.HTTP_BODY_ATTRIBUTE)) {
                      String body = (String) m.get(HttpRequestLogging.HTTP_BODY_ATTRIBUTE);
                      return convertedValue.equals(body);
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestAll() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(true, true);

    HttpRequestLogging.logHttpRequest(
        mockHttpServletRequest,
        true,
        null,
        false,
        null,
        true,
        null,
        ALT_REAL_CLIENT_IP_HEADER,
        ALT_CLIENT_ID_COOKIE,
        TEST_CLIENT_SESSION_ID,
        TEST_LOGIN_ID);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if ( TEST_REAL_CLIENT_IP.equals(m.get(HttpRequestLogging.REAL_CLIENT_IP_ATTRIBUTE)) &&
                        TEST_CLIENT_ID.equals(m.get(HttpRequestLogging.CLIENT_ID_ATTRIBUTE)) &&
                        TEST_CLIENT_SESSION_ID.equals(m.get(HttpRequestLogging.CLIENT_SESSION_ID_ATTRIBUTE)) &&
                        TEST_LOGIN_ID.equals(m.get(HttpRequestLogging.LOGIN_ID_ATTRIBUTE))
                        ) {
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }

  @Test
  public void logHttpRequestNone() throws InterruptedException, IOException {
    Mockito.reset(mockAppender);

    HttpServletRequest mockHttpServletRequest = getMockHttpServletRequest(false, false);

    HttpRequestLogging.logHttpRequest(
        mockHttpServletRequest,
        true,
        null,
        false,
        null,
        true,
        null,
        ALT_REAL_CLIENT_IP_HEADER,
        ALT_CLIENT_ID_COOKIE,
        null,
        null);

    Mockito.verify(mockAppender).doAppend(
        Mockito.argThat(new ArgumentMatcher<Object>() {
          @Override
          public boolean matches(final Object argument) {
            if (argument instanceof LoggingEvent) {
              LoggingEvent loggingEvent = (LoggingEvent) argument;
              Marker marker = loggingEvent.getMarker();
              if (marker instanceof MapEntriesAppendingMarker) {
                MapEntriesAppendingMarker mapEntriesAppendingMarker = (MapEntriesAppendingMarker) marker;
                try {
                  Object f = FieldUtils.readDeclaredField(mapEntriesAppendingMarker, "map", true);
                  if (f instanceof Map) {
                    Map<?, ?> m = (Map<?, ?>) f;
                    if ( m.get(HttpRequestLogging.REAL_CLIENT_IP_ATTRIBUTE) == null &&
                        m.get(HttpRequestLogging.CLIENT_ID_ATTRIBUTE) == null &&
                        m.get(HttpRequestLogging.CLIENT_SESSION_ID_ATTRIBUTE) == null &&
                        m.get(HttpRequestLogging.LOGIN_ID_ATTRIBUTE) == null 
                        ) {
                      return true;
                    }
                  }
                } catch (IllegalAccessException e) {
                  e.printStackTrace();
                }
              }
            }
            return false;
          }
        }));
  }
  
  @Test
  public void getMessageIdFromMDCTest(){
    MDC.put(HttpRequestLogging.MESSAGE_ID_ATTRIBUTE, TEST_MESSAGEID);
    final String messageId = HttpRequestLogging.getMessageId();
    
    assert(TEST_MESSAGEID.equals(messageId));
  }

  @Test
  public void getMessageIdNewTest(){
    MDC.clear();
    final String message_id = HttpRequestLogging.getMessageId();
    assert (message_id.matches(
        "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$"));
  }

  
  private HttpServletRequest getMockHttpServletRequest(boolean altvals, boolean setallvals) throws IOException {
    HttpServletRequest mockHttpServletRequest = Mockito.mock(
        HttpServletRequest.class,
        Mockito.RETURNS_DEEP_STUBS);
    Mockito.doReturn(TEST_CONTEXT_PATH).when(mockHttpServletRequest).getRequestURI();
    Mockito.doReturn(TEST_HTTP_METHOD).when(mockHttpServletRequest).getMethod();
    Mockito.doReturn(TEST_PEER_IP).when(mockHttpServletRequest).getRemoteAddr();

    final Map<String,String> headers = getBaseHeaders();

    if (setallvals) {
      Cookie mockCookie = Mockito.mock(Cookie.class);
      Mockito.doReturn(TEST_CLIENT_ID).when(mockCookie).getValue();
      if (altvals) {
        Mockito.doReturn(ALT_CLIENT_ID_COOKIE).when(mockCookie).getName();
        headers.put(ALT_REAL_CLIENT_IP_HEADER, TEST_REAL_CLIENT_IP);
      } else {
        Mockito.doReturn(HttpRequestLogging.CLIENT_ID_COOKIE).when(mockCookie).getName();
        headers.put(HttpRequestLogging.REAL_CLIENT_IP_HEADER, TEST_REAL_CLIENT_IP);
      }
      Mockito.when(mockHttpServletRequest.getCookies()).thenReturn(new Cookie[]{mockCookie});
    }
        
    ServletInputStream mockServletInputStream = Mockito.mock(ServletInputStream.class);
    Mockito.when(mockHttpServletRequest.getInputStream()).thenReturn(
        new ResettableServletInputStream(new ByteArrayInputStream(TEST_BODY.getBytes())));

    Mockito.when(mockHttpServletRequest.getHeaderNames()).then(new Answer<Enumeration<String>>() {
      @Override
      public Enumeration<String> answer(InvocationOnMock invocation) throws Throwable {
        return IteratorUtils.asEnumeration(headers.keySet().iterator());
      }
    });
    Mockito.when(mockHttpServletRequest.getHeader(Matchers.anyString())).then(new Answer<String>() {
      @Override
      public String answer(InvocationOnMock invocation) throws Throwable {
        return headers.get(invocation.getArgumentAt(0, String.class));
      }
    });

    return mockHttpServletRequest;
  }

  private HttpServletResponse getMockHttpServletResponse() {
    HttpServletResponse mockHttpServletResponse = Mockito.mock(
        HttpServletResponse.class,
        Mockito.RETURNS_DEEP_STUBS);
    Mockito.doReturn(returnCode).when(mockHttpServletResponse).getStatus();

    final Map<String,String> headers = getBaseHeaders();
    
    Mockito.when(mockHttpServletResponse.getHeaderNames()).then(new Answer<Collection<String>>() {
      @Override
      public Collection<String> answer(InvocationOnMock invocation) throws Throwable {
        return headers.keySet();
      }
    });
    Mockito.when(mockHttpServletResponse.getHeader(Matchers.anyString())).then(
        new Answer<String>() {
          @Override
          public String answer(InvocationOnMock invocation) throws Throwable {
            return headers.get(invocation.getArgumentAt(0, String.class));
          }
        });

    return mockHttpServletResponse;
  }

  private static Map<String,String> getBaseHeaders() {
    final Map<String, String> headers = new HashMap<>();
    headers.put("test1", "value1");
    headers.put("test2", "value2");
    headers.put("test3", "value3");
    headers.put("User-Agent", TEST_USER_AGENT);
    return headers;
  }
}
