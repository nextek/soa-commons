package com.sample.soa.context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.sample.soa.context.TxnContext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TxnContextTest {
  private HttpHeaders requestHeaders;
  private TxnContext context;

  @Before()
  public void setUp() {
    requestHeaders = mock(HttpHeaders.class);
    context = new TxnContext(requestHeaders);
  }

  @Test
  public void testSetContext1() {
    Map<String, String> params = new HashMap<>();
    params.put("Version", "1.5");
    MediaType mt = new MediaType("application", "json", params);
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertEquals("1.5", version);
  }

  @Test
  public void testSetContext2() {
    Map<String, String> params = new HashMap<>();
    params.put("version", "0.1.2");
    MediaType mt = new MediaType("application", "json", params);
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertEquals("0.1.2", version);
  }

  @Test
  public void testSetContext3() {
    Map<String, String> params = new HashMap<>();
    params.put("junk", "hello");
    params.put("version", "5");
    MediaType mt = new MediaType("application", "json", params);
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertEquals("5", version);
  }

  @Test
  public void testSetContext4() {
    Map<String, String> params = new HashMap<>();
    params.put("VERSION", "4.1.3");
    MediaType mt = new MediaType("application", "json", params);
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertEquals("4.1.3", version);
  }

  @Test
  public void testSetContext5() {
    Map<String, String> params = new HashMap<>();
    params.put("versio", "3");
    MediaType mt = new MediaType("application", "json", params);
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertNull(version);
  }

  @Test
  public void testSetContext6() {
    MediaType mt = new MediaType("application", "json");
    String version = TxnContext.getRequestVersionOfMediaType(mt);
    assertNull(version);
  }

  @Test()
  public void testGetRequestHeaders() {
    final HttpHeaders requestHeaders = mock(HttpHeaders.class);
    final TxnContext context = new TxnContext(requestHeaders);

    assertNotNull(context.getRequestHeaders());
  }

  @Test()
  public void testGetPreferredLanguage1() {
    final Locale locale = new Locale("*");

    when(requestHeaders.getAcceptableLanguages()).thenReturn(Collections.singletonList(locale));

    final Locale lang = context.getPreferredLanguage();

    assertNotNull(lang);
    assertEquals(lang.getLanguage(), "en");
    assertEquals(lang.getCountry(),  "US");
  }

  @Test()
  public void testGetPreferredLanguage2() {
    final Locale locale = new Locale("*");

    when(requestHeaders.getAcceptableLanguages()).thenReturn(Collections.singletonList(locale));

    Locale dfltLang = TxnContext.getDefaultNoRequestLanguage();
    TxnContext.setDefaultNoRequestLanguage(Locale.FRANCE);
    final Locale lang = context.getPreferredLanguage();
    TxnContext.setDefaultNoRequestLanguage(dfltLang);  // put it back

    assertNotNull(lang);
    assertEquals(lang, Locale.FRANCE);
  }

  @Test()
  public void testGetAcceptableLanguages1() {
    final Locale locale = new Locale("*");

    when(requestHeaders.getAcceptableLanguages()).thenReturn(Collections.singletonList(locale));

    final List<Locale> languages = context.getAcceptableLanguages();
    
    assertTrue(languages.size() > 0);

    Locale firstLang = languages.get(0);
    assertNotNull(firstLang);
    assertEquals(firstLang.getLanguage(), "en");
    assertEquals(firstLang.getCountry(),  "US");
  }

  @Test()
  public void testGetAcceptableLanguages2() {
    final Locale locale = new Locale("*");

    when(requestHeaders.getAcceptableLanguages()).thenReturn(Collections.singletonList(locale));

    Locale dfltLang = TxnContext.getDefaultNoRequestLanguage();
    TxnContext.setDefaultNoRequestLanguage(Locale.FRANCE);
    final List<Locale> languages = context.getAcceptableLanguages();
    TxnContext.setDefaultNoRequestLanguage(dfltLang);  // put it back

    assertTrue(languages.size() > 0);

    Locale firstLang = languages.get(0);
    assertEquals(firstLang, Locale.FRANCE);
  }

  @Test()
  public void testGetAcceptableLanguages3() {
    final Locale locale1 = new Locale("*");
    final Locale locale2 = Locale.FRANCE;
    final Locale locale3 = new Locale("de");

    List<Locale> retList = new ArrayList<>();
    retList.add(locale1);
    retList.add(locale2);
    retList.add(locale3);
    when(requestHeaders.getAcceptableLanguages()).thenReturn(retList);

    final List<Locale> languages = context.getAcceptableLanguages();

    assertEquals(3, languages.size());
    assertEquals(languages.get(0), locale1);
    assertEquals(languages.get(1), locale2);
    assertEquals(languages.get(2), locale3);
  }

  @Test()
  public void testGetRequestContentLanguage1() {
    when(requestHeaders.getLanguage()).thenReturn(null);

    final Locale lang = context.getRequestContentLanguage();

    assertNotNull(lang);
    assertEquals(lang.getLanguage(), "en");
    assertEquals(lang.getCountry(),  "US");
  }

  @Test()
  public void testGetRequestContentLanguage2() {
    when(requestHeaders.getLanguage()).thenReturn(new Locale("de"));

    final Locale lang = context.getRequestContentLanguage();

    assertNotNull(lang);
    assertEquals(lang.getLanguage(), "de");
  }

  @Test()
  public void testGetAcceptableMediaTypes() {
    when(requestHeaders.getAcceptableMediaTypes())
        .thenReturn(Collections.singletonList(MediaType.WILDCARD_TYPE));

    final List<MediaType> mediaTypeList = context.getAcceptableMediaTypes();
    assertTrue(mediaTypeList.size() > 0);
  }

  @Test()
  public void testUpdateResponse1() {
    final Locale locale = Locale.US;
    context.setResponseContentLanguage(locale);

    final Response.ResponseBuilder builder = mock(Response.ResponseBuilder.class);
    context.updateResponse(builder);

    verify(builder).language(locale);
  }

  @Test()
  public void testUpdateResponse2() {
    final Locale locale = Locale.US;
    context.setResponseContentLanguage(locale);
    final Date lastModifiedVal = new Date(2016, 6, 11, 22, 30, 1);
    context.setLastModified(lastModifiedVal);

    final Response.ResponseBuilder builder = mock(Response.ResponseBuilder.class);
    context.updateResponse(builder);

    verify(builder).language(locale);
    verify(builder).lastModified(lastModifiedVal);
  }

  @Test()
  public void testGetPreferredVersion1() {
    Map<String, String> params = new HashMap<>();
    params.put("Version", "2");
    final MediaType mt = new MediaType("application", "json", params);

    when(requestHeaders.getAcceptableMediaTypes()).thenReturn(Collections.singletonList(mt));
    final String version = context.getPreferredVersion();
    assertEquals("2", version);
  }

  @Test()
  public void testGetPreferredVersion2() {
    Map<String, String> params = new HashMap<>();
    final MediaType mt = new MediaType("application", "json", params);

    when(requestHeaders.getAcceptableMediaTypes()).thenReturn(Collections.singletonList(mt));
    final String version = context.getPreferredVersion();
    assertNull(version);
  }

  @Test()
  public void testGetAcceptableVersions1() {
    Map<String, String> params1 = new HashMap<>();
    params1.put("Version", "1.1");
    final MediaType mt1 = new MediaType("application", "json", params1);

    Map<String, String> params2 = new HashMap<>();
    params2.put("Version", "2.2");
    final MediaType mt2 = new MediaType("text", "html", params2);

    Map<String, String> params3 = new HashMap<>();
    params3.put("Version", "3.3");
    final MediaType mt3 = new MediaType("application", "xml", params3);

    List<MediaType> mtList = new ArrayList<>();
    mtList.add(mt1);
    mtList.add(mt3);

    when(requestHeaders.getAcceptableMediaTypes()).thenReturn(mtList);
    final List<String> versions = context.getAcceptableVersions();

    assertEquals(2, versions.size());
    assertEquals("1.1", versions.get(0));
    assertEquals("3.3", versions.get(1));
  }

}
