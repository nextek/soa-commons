package com.sample.soa.errors.mappers;

import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.sample.soa.errors.mappers.JsonMappingExceptionMapper;
import com.sample.soa.errors.response.ErrorDetail;
import com.sample.soa.errors.response.Notification;

public class JsonMappingExceptionMapperTest {
  private JsonMappingExceptionMapper excMapper;
  private DummyClass                 dummyClass;

  @Before()
  public void setUp() {
    excMapper  = new JsonMappingExceptionMapper();
    dummyClass = new DummyClass();
  }

  @Test
  public void test1() {
    JsonMappingException e = new JsonMappingException("failed to map json");
    e.prependPath(dummyClass, "preferred");
    e.prependPath(dummyClass, 0);
    e.prependPath(dummyClass, "addresses");
    e.prependPath(dummyClass, "personalinfo");
    e.prependPath(dummyClass, "guests");

    Response r = excMapper.toResponse(e);
    Object entity = r.getEntity();
    assertEquals("com.sample.soa.errors.response.ErrorDetail", entity.getClass().getName());

    if("com.sample.soa.errors.response.ErrorDetail".equals(entity.getClass().getName())) {
      ErrorDetail errorDetail = (ErrorDetail) entity;
      List<Notification> n = errorDetail.getNotifications();

      assertEquals(1, n.size());
      if(n.size() >= 1) {
        List<String> fields = n.get(0).getFields();
        assertEquals(1, fields.size());
        if(fields.size() > 0) {
          String field = fields.get(0);
          assertEquals("guests.personalinfo.addresses.preferred", field);
        }
      }
    }
  }

  @Test
  public void test2() {
    JsonMappingException e = new JsonMappingException("failed to map json");
    e.prependPath(dummyClass, "partnerCode");
    e.prependPath(dummyClass, 3);

    Response r = excMapper.toResponse(e);
    Object entity = r.getEntity();
    assertEquals("com.sample.soa.errors.response.ErrorDetail", entity.getClass().getName());

    if("com.sample.soa.errors.response.ErrorDetail".equals(entity.getClass().getName())) {
      ErrorDetail errorDetail = (ErrorDetail) entity;
      List<Notification> n = errorDetail.getNotifications();

      assertEquals(1, n.size());
      if(n.size() >= 1) {
        List<String> fields = n.get(0).getFields();
        assertEquals(1, fields.size());
        if(fields.size() > 0) {
          String field = fields.get(0);
          assertEquals("partnerCode", field);
        }
      }
    }
  }

  private class DummyClass {
  }
}
