package com.sample.soa.errors.mappers;

import java.lang.Class;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;
import javax.validation.Path.Node;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import com.sample.soa.errors.mappers.ConstraintViolationExceptionMapper;
import com.sample.soa.errors.response.ErrorDetail;
import com.sample.soa.errors.response.Notification;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConstraintViolationExceptionMapperTest {
  private ConstraintViolationExceptionMapper excMapper;

  @Before()
  public void setUp() {
    excMapper  = new ConstraintViolationExceptionMapper();
  }

  public void testHelper(Path mockPath, String wantPathString) {
    TestAnnot testAnnot = new TestAnnot()
      {
        @Override
        public String foo()
        {
          return "foo";
        }

        @Override
        public Class<? extends Annotation> annotationType()
        {
          return TestAnnot.class;
        }
      };

    ConstraintDescriptor mockDescr = mock(ConstraintDescriptor.class);
    when(mockDescr.getAnnotation()).thenReturn(testAnnot);

    ConstraintViolation<String> mockConstrViol
      = (ConstraintViolation<String>)mock(ConstraintViolation.class);
    when(mockConstrViol.getPropertyPath()).thenReturn(mockPath);
    when(mockConstrViol.getConstraintDescriptor()).thenReturn(mockDescr);

    Set<ConstraintViolation<?>> constraintSet = new HashSet<>();
    constraintSet.add(mockConstrViol);

    ConstraintViolationException mockConstrViolExc = mock(ConstraintViolationException.class);
    when(mockConstrViolExc.getConstraintViolations()).thenReturn(constraintSet);

    Response r = excMapper.toResponse(mockConstrViolExc);
    Object entity = r.getEntity();
    assertEquals("com.sample.soa.errors.response.ErrorDetail", entity.getClass().getName());
    if("com.sample.soa.errors.response.ErrorDetail".equals(entity.getClass().getName())) {
      ErrorDetail errorDetail = (ErrorDetail) entity;
      List<Notification> n = errorDetail.getNotifications();

      assertEquals(1, n.size());
      if(n.size() >= 1) {
        List<String> fields = n.get(0).getFields();
        assertEquals(1, fields.size());
        if(fields.size() > 0) {
          String field = fields.get(0);
          assertEquals(wantPathString, field);
        }
      }
    }
  }

  @Test
  public void test1() {
    Path.Node mockPathNode1 = mock(Path.Node.class);
    when(mockPathNode1.getName()).thenReturn("guests");

    Path.Node mockPathNode2 = mock(Path.Node.class);
    when(mockPathNode2.getName()).thenReturn("personalinfo");

    Path.Node mockPathNode3 = mock(Path.Node.class);
    when(mockPathNode3.getName()).thenReturn("emails");

    Path.Node mockPathNode4 = mock(Path.Node.class);
    when(mockPathNode4.getName()).thenReturn("emailAddress");

    Iterator mockIter = mock(Iterator.class);
    when(mockIter.hasNext()).thenReturn(true, true, true, true, false);
    when(mockIter.next()).thenReturn(mockPathNode1, mockPathNode2, mockPathNode3, mockPathNode4);

    Path mockPath = mock(Path.class);
    when(mockPath.iterator()).thenReturn(mockIter);
    testHelper(mockPath, "guests.personalinfo.emails.emailAddress");
  }

  @Test
  public void test2() {
    Path.Node mockPathNode1 = mock(Path.Node.class);
    when(mockPathNode1.getName()).thenReturn("guests");

    Path.Node mockPathNode2 = mock(Path.Node.class);
    when(mockPathNode2.getName()).thenReturn("personalinfo");

    Path.Node mockPathNode3 = mock(Path.Node.class);
    when(mockPathNode3.getName()).thenReturn("");

    Path.Node mockPathNode4 = mock(Path.Node.class);
    when(mockPathNode4.getName()).thenReturn("emails");

    Iterator mockIter = mock(Iterator.class);
    when(mockIter.hasNext()).thenReturn(true, true, true, true, false);
    when(mockIter.next()).thenReturn(mockPathNode1, mockPathNode2, mockPathNode3, mockPathNode4);

    Path mockPath = mock(Path.class);
    when(mockPath.iterator()).thenReturn(mockIter);
    testHelper(mockPath, "guests.personalinfo.emails");
  }

  @Test
  public void test3() {
    Path.Node mockPathNode1 = mock(Path.Node.class);
    when(mockPathNode1.getName()).thenReturn("guests");

    Path.Node mockPathNode2 = mock(Path.Node.class);
    when(mockPathNode2.getName()).thenReturn("personalinfo");

    Path.Node mockPathNode3 = mock(Path.Node.class);
    when(mockPathNode3.getName()).thenReturn(null);

    Path.Node mockPathNode4 = mock(Path.Node.class);
    when(mockPathNode4.getName()).thenReturn("emails");

    Iterator mockIter = mock(Iterator.class);
    when(mockIter.hasNext()).thenReturn(true, true, true, true, false);
    when(mockIter.next()).thenReturn(mockPathNode1, mockPathNode2, mockPathNode3, mockPathNode4);

    Path mockPath = mock(Path.class);
    when(mockPath.iterator()).thenReturn(mockIter);
    testHelper(mockPath, "guests.personalinfo.emails");
  }

  public @interface TestAnnot {
    String foo();
  }
}


