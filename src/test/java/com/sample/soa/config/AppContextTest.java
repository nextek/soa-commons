package com.sample.soa.config;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sample.soa.config.ErrorContext;

public class AppContextTest {

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Test
  public void testSetContext() {
    String myContext="TestCommon";
    ErrorContext.setContext(myContext);

    assert(myContext.equals(ErrorContext.getContext()));
  }

  @Test
  public void testGetContext() {
    assert("TestCommon".equals(ErrorContext.getContext()));
  }

}
