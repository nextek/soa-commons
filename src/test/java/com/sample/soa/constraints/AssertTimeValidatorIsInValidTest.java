package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertTimeValidatorIsInValidTest {
  private DateTimeTarget dateTimeTarget;
  private Validator validator;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new DateTimeTarget(null, null, "-00:00:00") },
        { new DateTimeTarget(null, null, "24:00:00") },
    });
  }

  public AssertTimeValidatorIsInValidTest(DateTimeTarget dateTimeTarget) {
    this.dateTimeTarget = dateTimeTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<DateTimeTarget>> constraintViolations =
        validator.validate(dateTimeTarget);

    assertEquals(1, constraintViolations.size());
    assertEquals("Invalid time.", constraintViolations.iterator().next().getMessage());
  }
}