package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertDateTimeValidatorIsInValidTest {
  private DateTimeTarget dateTimeTarget;
  private Validator validator;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new DateTimeTarget("2016-01-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-01-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-02-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2015-02-29T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-02-30T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-03-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-03-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-04-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-04-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-05-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-05-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-06-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-06-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-07-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-07-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-08-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-08-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-09-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-09-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-10-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-10-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-11-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-11-32T00:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-12-01T24:00:00+00:00", null, null) },
        { new DateTimeTarget("2016-12-32T00:00:00+00:00", null, null) },
    });
  }

  public AssertDateTimeValidatorIsInValidTest(DateTimeTarget dateTimeTarget) {
    this.dateTimeTarget = dateTimeTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<DateTimeTarget>> constraintViolations =
        validator.validate(dateTimeTarget);

    assertEquals(1, constraintViolations.size());
    assertEquals("Invalid date time.",
        constraintViolations.iterator().next().getMessage());
  }
}