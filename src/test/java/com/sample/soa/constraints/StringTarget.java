/**
 * 
 */
package com.sample.soa.constraints;

import java.util.List;
import java.util.Set;

import com.sample.soa.constraints.NotEmpty;

/**
 * @author Chuck Ludwigsen
 * @version 1
 *
 */
public class StringTarget {

  @NotEmpty()
  private String testString;

  @NotEmpty()
  private String[] testArray;

  @NotEmpty()
  private List<String> testList;

  @NotEmpty()
  private Set<String> testSet;

  public StringTarget() {
    // Empty
  }

  public StringTarget(String testString, String[] testArray,
      List<String> testList, Set<String> testSet) {
    this.testString = testString;
    this.testArray = testArray;
    this.testList = testList;
    this.testSet = testSet;
  }

  /**
   * @return the testString
   */
  public final String getTestString() {
    return testString;
  }

  /**
   * @param testString the testString to set
   */
  public final void setTestString(String testString) {
    this.testString = testString;
  }

  /**
   * @return the testArray
   */
  public final String[] getTestArray() {
    return testArray;
  }

  /**
   * @param testArray the testArray to set
   */
  public final void setTestArray(String[] testArray) {
    this.testArray = testArray;
  }

  /**
   * @return the testList
   */
  public final List<String> getTestList() {
    return testList;
  }

  /**
   * @param testList the testList to set
   */
  public final void setTestList(List<String> testList) {
    this.testList = testList;
  }

  /**
   * @return the testSet
   */
  public final Set<String> getTestSet() {
    return testSet;
  }

  /**
   * @param testSet the testSet to set
   */
  public final void setTestSet(Set<String> testSet) {
    this.testSet = testSet;
  }

}
