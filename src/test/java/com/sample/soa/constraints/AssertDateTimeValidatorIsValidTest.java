package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertDateTimeValidatorIsValidTest {
  private DateTimeTarget dateTimeTarget;
  private Validator validator;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {new DateTimeTarget("2016-01-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-01-01T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-02-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2015-02-28T23:59:59+00:00", null, null)},

        // check for leap year
        {new DateTimeTarget("2016-02-29T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-03-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-03-31T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-04-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-04-30T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-05-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-05-31T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-06-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-06-30T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-07-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-07-31T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-08-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-08-31T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-09-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-09-30T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-10-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-10-31T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-11-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-11-30T23:59:59+00:00", null, null)},
        {new DateTimeTarget("2016-12-01T00:00:00+00:00", null, null)},
        {new DateTimeTarget("2016-12-31T23:59:59+00:00", null, null)},

        // check for leap second
        // {new DateTimeTarget("2015-06-30T23:59:60Z", null, null)},

    });
  }

  public AssertDateTimeValidatorIsValidTest(DateTimeTarget dateTimeTarget) {
    this.dateTimeTarget = dateTimeTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<DateTimeTarget>> constraintViolations =
        validator.validate(dateTimeTarget);

    assertEquals(0, constraintViolations.size());
  }
}