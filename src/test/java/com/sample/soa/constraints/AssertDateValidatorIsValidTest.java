package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertDateValidatorIsValidTest {
  private DateTimeTarget dateTimeTarget;
  private Validator validator;

  @Parameterized.Parameters()
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new DateTimeTarget(null, "2016-01-01", null) },
        { new DateTimeTarget(null, "2016-01-31", null) },
        { new DateTimeTarget(null, "2016-02-01", null) },
        { new DateTimeTarget(null, "2016-02-28", null) },
        { new DateTimeTarget(null, "2016-02-29", null) },
        { new DateTimeTarget(null, "2016-03-01", null) },
        { new DateTimeTarget(null, "2016-03-31", null) },
        { new DateTimeTarget(null, "2016-04-01", null) },
        { new DateTimeTarget(null, "2016-04-30", null) },
        { new DateTimeTarget(null, "2016-05-01", null) },
        { new DateTimeTarget(null, "2016-05-31", null) },
        { new DateTimeTarget(null, "2016-06-01", null) },
        { new DateTimeTarget(null, "2016-06-30", null) },
        { new DateTimeTarget(null, "2016-07-01", null) },
        { new DateTimeTarget(null, "2016-07-31", null) },
        { new DateTimeTarget(null, "2016-08-01", null) },
        { new DateTimeTarget(null, "2016-08-31", null) },
        { new DateTimeTarget(null, "2016-09-01", null) },
        { new DateTimeTarget(null, "2016-09-30", null) },
        { new DateTimeTarget(null, "2016-10-01", null) },
        { new DateTimeTarget(null, "2016-10-31", null) },
        { new DateTimeTarget(null, "2016-11-01", null) },
        { new DateTimeTarget(null, "2016-11-30", null) },
        { new DateTimeTarget(null, "2016-12-01", null) },
        { new DateTimeTarget(null, "2016-12-31", null) },
    });
  }

  public AssertDateValidatorIsValidTest(DateTimeTarget dateTimeTarget) {
    this.dateTimeTarget = dateTimeTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<DateTimeTarget>> constraintViolations =
        validator.validate(dateTimeTarget);

    assertEquals(0, constraintViolations.size());
  }
}