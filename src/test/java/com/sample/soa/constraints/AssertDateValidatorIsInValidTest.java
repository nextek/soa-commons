package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertDateValidatorIsInValidTest {
  private DateTimeTarget dateTimeTarget;
  private Validator validator;

  /**
   *
   * @return Collection
   */
  @Parameterized.Parameters()
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new DateTimeTarget(null, "2016-01-32", null) },
        { new DateTimeTarget(null, "2016-02-30", null) },
        { new DateTimeTarget(null, "2016-03-32", null) },
        { new DateTimeTarget(null, "2016-04-31", null) },
        { new DateTimeTarget(null, "2016-05-32", null) },
        { new DateTimeTarget(null, "2016-06-31", null) },
        { new DateTimeTarget(null, "2016-07-32", null) },
        { new DateTimeTarget(null, "2016-08-32", null) },
        { new DateTimeTarget(null, "2016-09-31", null) },
        { new DateTimeTarget(null, "2016-10-32", null) },
        { new DateTimeTarget(null, "2016-11-31", null) },
        { new DateTimeTarget(null, "2016-12-32", null) },
    });
  }

  /**
   *
   * @param dateTimeTarget DateTimeTarget
   */
  public AssertDateValidatorIsInValidTest(final DateTimeTarget dateTimeTarget) {
    this.dateTimeTarget = dateTimeTarget;
  }

  /**
   *
   */
  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  /**
   *
   * @throws Exception e
   */
  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<DateTimeTarget>> constraintViolations =
        validator.validate(dateTimeTarget);

    assertEquals(1, constraintViolations.size());
    assertEquals("Invalid date.", constraintViolations.iterator().next().getMessage());
  }
}