package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertEmailValidatorIsInValidTest {
  private EmailTarget emailTarget;
  private Validator validator;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new EmailTarget("Abc.example.com") },
        { new EmailTarget("A@b@c@example.com") },
        { new EmailTarget("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com") },
        { new EmailTarget("just\"not\"right@example.com") },
        { new EmailTarget("this is\"not\\allowed@example.com") },
        //{ new EmailTarget("this\\ still\\\"not\\allowed@example.com") },
        { new EmailTarget("john..doe@example.com") },
        { new EmailTarget("john.doe@example..com") },
        { new EmailTarget("012345678901234567890123456789012345678901234567890123456"
                        + "78901234@ab.com") },

        { new EmailTarget("plainaddress") },
        { new EmailTarget("#@%^%#$@#$@#.com") },
        { new EmailTarget("@example.com") },
        { new EmailTarget("Joe Smith <email@example.com>") },
        { new EmailTarget("email.example.com") },
        { new EmailTarget("email@example@example.com") },
        { new EmailTarget(".email@example.com") },
        { new EmailTarget("email.@example.com") },
        { new EmailTarget("email..email@example.com") },
        { new EmailTarget("email@example.com (Joe Smith)") },
        { new EmailTarget("email@example") },
        { new EmailTarget("email@-example.com") },
        { new EmailTarget("email@example.web") },
        { new EmailTarget("email@111.222.333.44444") },
        { new EmailTarget("email@example..com") },
      });
  }

  public AssertEmailValidatorIsInValidTest(EmailTarget emailTarget) {
    this.emailTarget = emailTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<EmailTarget>> constraintViolations =
        validator.validate(emailTarget);

    assertEquals(1, constraintViolations.size());
    assertEquals("Invalid email.",
        constraintViolations.iterator().next().getMessage());
  }
}
