package com.sample.soa.constraints;

import com.sample.soa.constraints.AssertValidEmail;

/**
 * Used for testing constraint annotations AssertValidEmail.
 *
 * @author Brandon Schendel
 * @version 1.0
 */
public class EmailTarget {
  @AssertValidEmail()
  private String emailString;

  public EmailTarget() {
  }

  public EmailTarget(String emailString) {
    this.emailString = emailString;
  }

  public String getEmailString() {
    return emailString;
  }

  public void setEmailString(String emailString) {
    this.emailString = emailString;
  }
}
