package com.sample.soa.constraints;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class AssertEmailValidatorIsValidTest {
  private EmailTarget emailTarget;
  private Validator validator;

  @Parameterized.Parameters
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][]{
        {new EmailTarget("mail@brandon.schendel.name")},
        {new EmailTarget("testuser@gmail.com")},
        {new EmailTarget("0123456789@test.com")},
        {new EmailTarget("JoHnDoE@test.com")},

        {new EmailTarget("J.O.H.N.D.O.E@test.com")},
        //{new EmailTarget("john.smith(comment)@example.com")},
        {new EmailTarget("abc.\"defghi\".xyz@example.com")},
        {new EmailTarget("\"abcdefghixyz\"@example.com")},
        {new EmailTarget("!#$%&'*+-/=?^_`{|}~@example.com")},

        {new EmailTarget("mason@日本.com")},
        {new EmailTarget("wildwezyr@fahrvergnügen.net")},

        {new EmailTarget("\"Abc\\@def\"@example.com")},
        {new EmailTarget("\"Fred Bloggs\"@example.com")},
        {new EmailTarget("\"Joe\\Snow\"@example.com")},
        {new EmailTarget("\"Abc@def\"@example.com")},
        {new EmailTarget("customer/department=shipping@example.com")},
        {new EmailTarget("\\$A12345@example.com")},
        {new EmailTarget("!def!xyz%abc@example.com")},
        {new EmailTarget("_somename@example.com")},        
        {new EmailTarget("0123456789012345678901234567890123456789012345678901234567890123@ab.com"
                         )},
        {new EmailTarget("disposable.style.email.with+symbol@example.com")},
        {new EmailTarget("other.email-with-dash@example.com")},
        {new EmailTarget("x@example.com")},
        {new EmailTarget("\"much.more unusual\"@example.com")},
        {new EmailTarget("\"very.unusual.@.unusual.com\"@example.com")},
        {new EmailTarget("example-indeed@strange-example.com")},
        {new EmailTarget("#!$%&'*+-/=?^_`{}|~@example.org")},
        {new EmailTarget("\"()<>[]:,;@\\\"!#$%&'*+-/=?^_`{}| ~.a\"@example.org")},
        {new EmailTarget("\" \"@example.org")},
        {new EmailTarget("example@s.solutions")},
      });
  }

  public AssertEmailValidatorIsValidTest(EmailTarget emailTarget) {
    this.emailTarget = emailTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<EmailTarget>> constraintViolations =
        validator.validate(emailTarget);

    assertEquals(0, constraintViolations.size());
  }
}
