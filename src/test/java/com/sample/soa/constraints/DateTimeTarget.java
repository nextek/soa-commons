package com.sample.soa.constraints;

import com.sample.soa.constraints.AssertDate;
import com.sample.soa.constraints.AssertDateTime;
import com.sample.soa.constraints.AssertTime;

/**
 * Used for testing constraint annotations AssertDateTime, AssertDate and AssertTime.
 *
 * @author Sean Alphonse
 * @version 1.0
 */
public class DateTimeTarget {
  @AssertDateTime()
  private String dateTimeString;

  @AssertDate()
  private String dateString;

  @AssertTime()
  private String timeString;

  public DateTimeTarget() {
  }

  public DateTimeTarget(String dateTimeString, String dateString, String timeString) {
    this.dateTimeString = dateTimeString;
    this.dateString = dateString;
    this.timeString = timeString;
  }

  public String getDateTimeString() {
    return dateTimeString;
  }

  public void setDateTimeString(String dateTimeString) {
    this.dateTimeString = dateTimeString;
  }

  public String getDateString() {
    return dateString;
  }

  public void setDateString(String dateString) {
    this.dateString = dateString;
  }

  public String getTimeString() {
    return timeString;
  }

  public void setTimeString(String timeString) {
    this.timeString = timeString;
  }
}
