package com.sample.soa.constraints;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class NotEmptyValidatorIsValidTest {
  private StringTarget stringTarget;
  private Validator validator;

  @Parameterized.Parameters()
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        { new StringTarget(null, null, null, null) }
    });
  }

  public NotEmptyValidatorIsValidTest(StringTarget stringTarget) {
    this.stringTarget = stringTarget;
  }

  @Before()
  public void setUp() {
    final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  @Test
  public void testIsValid() throws Exception {
    final Set<ConstraintViolation<StringTarget>> constraintViolations =
        validator.validate(stringTarget);

    assertEquals(4, constraintViolations.size());
  }
}
